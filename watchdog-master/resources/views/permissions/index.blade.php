@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="d-flex justify-content-between">
        <div>
            <h1>Permissions:</h1>
        </div>
        <div>
            <a href="/admin" class="btn btn-secondary pr-3"><i class="material-icons">arrow_back</i> Admin </a>
            <button type="button" class="btn btn-primary pr-3" data-toggle="modal" data-target="#addModal"><i class="material-icons">add</i> Add </button>
        </div>
    </div>

    @include('partials.flashMessages')

    <div class="mt-2 row justify-content-center">
        <table class="table table-striped">
            <thead class="bg-dark text-white">
                <tr>
                    <th colspan="2" class="text-center" style="width: 16.66%">Action</th>
                    <th scope="col">Name</th>
                    <th scope="col">Label</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($permissions as $permission)
                <tr>
                    <td class="text-center">
                        <a href="#" class="text-info" title="Edit Permission" data-toggle="modal" 
                            data-target="#editModal" data-id="{{$permission->id}}" data-name="{{$permission->name}}" 
                            data-label="{{$permission->label}}" data-action="{{$permission->path()}}">
                                <i class="material-icons">edit</i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="#" class="text-danger" title="Delete Permission" data-toggle="modal" 
                            data-target="#deleteModal" data-id="{{$permission->id}}" data-name="{{$permission->name}}" 
                            data-label="{{$permission->label}}" data-action="{{$permission->path()}}">
                                <i class="material-icons">delete</i>
                        </a>
                    </td>
                    <td scope="row">{{$permission->name}}</td>
                    <td scope="row">{{$permission->label}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {{ $permissions->links() }}

    @include('partials.permissions.addModal')
    @include('partials.permissions.deleteModal')
    @include('partials.permissions.editModal')
</div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection