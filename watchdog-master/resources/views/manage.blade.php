@extends('layouts.app')

@section('content')
    
    <div class="row">
        @can('edit_components', Auth::user())
        <div class="col-sm-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <h1>Components</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title">View and Edit Components</h5>
                    <p>This is where you can add components avilable for the dashboard.</p>
                    <a href="/components" class="btn btn-primary">Components</a>
                </div>
            </div>
        </div>
        @endcan
        @can('edit_alerts', Auth::user())
        <div class="col-sm-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <h1>Alerts</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title">View and Edit Alerts</h5>
                    <p>This is where you can configure the threshold of when an alert is triggered as well as what roles are alerted.</p>
                    <a href="/alerts" class="btn btn-primary">Alerts</a>
                </div>
            </div>
        </div>
        @endcan
    </div>

@endsection