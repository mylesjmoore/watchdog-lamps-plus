@extends('layouts.app')

@section('content')
    <div class="container">
        
        <div class="d-flex justify-content-between">
            <div>
                <h1>{{$role->name}}'s Permissions:</h1>
            </div>
            <div>
                <a href="/roles" class="btn btn-secondary pr-3">
                    <i class="material-icons">arrow_back</i> Roles 
                </a>
                <button type="button" class="btn btn-primary pr-3" 
                    data-toggle="modal" data-target="#addModal">
                        <i class="material-icons">add</i> Add Permission
                </button>
            </div>
        </div>

        @include('partials.flashMessages')

        <div class="mt-2 row justify-content-center">
            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr>
                        <th class="text-center" style="width: 16.66%">Action</th>
                        <th scope="col">Permission Name</th>
                        <th scope="col">Permission Label</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($role->permissions as $permission)
                    <tr>
                        <td class="text-center">
                            <a href="#" class="text-danger" title="Remove Permission" 
                                data-toggle="modal" data-target="#deleteModal" 
                                data-role-id="{{$role->id}}" data-permission-id="{{$permission->id}}" 
                                data-name="{{$permission->name}}" data-label="{{$permission->label}}"
                                data-action="/permissionrole/{{$permission->id}}/role/{{$role->id}}">
                                    <i class="material-icons">delete</i>
                            </a>
                        </td>
                        <td scope="row">{{$permission->name}}</td>
                        <td scope="row">{{$permission->label}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        @include('partials.permissionRoles.addModal')
        @include('partials.permissionRoles.deleteModal')
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection
