@extends('layouts.app')

@section('content')
    <div class="container">
        
        <div class="d-flex justify-content-between">
            <div>
                <h1>Roles:</h1>
            </div>
            <div>
                <a href="/admin" class="btn btn-secondary pr-3">
                    <i class="material-icons">arrow_back</i> Admin 
                </a>
                <button type="button" class="btn btn-primary pr-3" 
                    data-toggle="modal" data-target="#addModal">
                        <i class="material-icons">add</i> Add 
                </button>
            </div>
        </div>

        @include('partials.flashMessages')

        <div class="mt-2 row justify-content-center">
            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr >
                        <th colspan="3" class="text-center" style="width: 16.66%">Action</th>
                        <th scope="col">Name</th>
                        <th scope="col">Label</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($roles as $role)
                    <tr>
                        <td class="text-center">
                            <a href="{{$role->path()}}" class="text-info" title="View Permissions">
                                <i class="material-icons">visibility</i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" class="text-info" title="Edit Role" data-toggle="modal" 
                                data-target="#editModal" data-id="{{$role->id}}" data-name="{{$role->name}}" 
                                data-label="{{$role->label}}" data-action="{{$role->path()}}">
                                    <i class="material-icons">edit</i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" class="text-danger" title="Delete Role" data-toggle="modal" 
                                data-target="#deleteModal" data-id="{{$role->id}}" data-name="{{$role->name}}" 
                                data-label="{{$role->label}}" data-action="{{$role->path()}}">
                                    <i class="material-icons">delete</i>
                            </a>
                        </td>
                        <td scope="row">{{$role->name}}</td>
                        <td scope="row">{{$role->label}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        {{ $roles->links() }}

        @include('partials.roles.addModal')
        @include('partials.roles.deleteModal')
        @include('partials.roles.editModal')
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection