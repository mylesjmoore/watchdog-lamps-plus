<!-- edit modal -->
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalTitle">Edit Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit-form-action" action="" method="POST">
            @csrf
            @method('PATCH')
            <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-tag" class="col-form-label">Tag:</label>
                        <input type="text" class="form-control" id="edit-tag" name="tag" placeholder="example-component" required autofocus>
                        <small class="form-text text-muted">tag is required to be kebab case of component Vue file name.</small>
                        <small class="form-text text-muted">EG ./components/ExampleComponent.vue -> example-component</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-title" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="edit-title" name="title" placeholder="Example Chart" required>
                        <small class="form-text text-muted">This is the title that will be displayed to the user on the component.</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-size" class="col-form-label">Size:</label>
                        <select class="form-control" id="edit-size" name="size" required>
                            <option value="col-xl-1">col-xl-1</option>
                            <option value="col-xl-2">col-xl-2</option>
                            <option value="col-xl-3">col-xl-3</option>
                            <option value="col-xl-4">col-xl-4</option>
                            <option value="col-xl-5">col-xl-5</option>
                            <option value="col-xl-6">col-xl-6</option>
                            <option value="col-xl-7">col-xl-7</option>
                            <option value="col-xl-8">col-xl-8</option>
                            <option value="col-xl-9">col-xl-9</option>
                            <option value="col-xl-10">col-xl-10</option>
                            <option value="col-xl-11">col-xl-11</option>
                            <option value="col-xl-12">col-xl-12</option>
                        </select>
                        <small class="form-text text-muted">This is how much width the component will take up on the screen.</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-available" class="col-form-label">Available:</label>
                        <select class="form-control" id="edit-available" name="available" required>
                            <option value="1">True</option>
                            <option value="0">False</option>
                        </select>
                        <small class="form-text text-muted">Is this component avilable to users?</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-permission" class="col-form-label">Permission:</label>
                        <select class="form-control" id="edit-permission" name="permission">
                            @foreach($permissions as $permission)
                                <option value="{{$permission->id}}">{{$permission->name}}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">What permission is required for the user to view this component?</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>