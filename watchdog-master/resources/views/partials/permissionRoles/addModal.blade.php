<!-- add modal -->
<div class="modal fade" id="addModal" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalTitle">Add Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-form-action" action="/permissionrole/{{$role->id}}" method="POST">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-permission" class="col-form-label">Permission:</label>
                        <select class="form-control" id="add-permission" name="name">
                            @foreach($permissions as $permission)
                                <option>{{$permission->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>