<!-- edit modal -->
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalTitle">Edit Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit-form-action" action="" method="POST">
            @csrf
            @method('PATCH')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-name" class="col-form-label">Name:</label>
                        <input type="text" class="form-control" id="edit-name" name="name" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="edit-label" class="col-form-label">Label:</label>
                        <input type="text" class="form-control" id="edit-label" name="label" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>