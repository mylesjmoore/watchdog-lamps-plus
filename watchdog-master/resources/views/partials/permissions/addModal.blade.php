<!-- add modal -->
<div class="modal fade" id="addModal" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalTitle">Add Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-form-action" action="/permissions" method="POST">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-name" class="col-form-label">Name:</label>
                        <input type="text" class="form-control" id="add-name" name="name" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="add-label" class="col-form-label">Label:</label>
                        <input type="text" class="form-control" id="add-label" name="label" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>