<!-- edit modal -->
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalTitle">Edit Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit-form-action" action="" method="POST">
            @csrf
            @method('PATCH')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-type" class="col-form-label">Type:</label>
                        <input type="text" class="form-control" id="edit-type" name="type" readonly>
                        <small class="form-text text-muted">Alert Type</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-level" class="col-form-label">Level:</label>
                        <input type="text" class="form-control" id="edit-level" name="level" readonly>
                        <small class="form-text text-muted">Warning level</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-percent" class="col-form-label">Percent:</label>
                        <input type="text" class="form-control" id="edit-percent" name="percent">
                        <small class="form-text text-muted">At this percentage (0-100) an alert will be triggered.</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-threshold" class="col-form-label">Threshold:</label>
                        <input type="text" class="form-control" id="edit-threshold" name="threshold">
                        <small class="form-text text-muted">At this threshold an alert will be triggered.</small>
                    </div>
                    <div class="form-group">
                        <label for="edit-role" class="col-form-label">Alerted Role:</label>
                        <select class="form-control" id="edit-role" name="role">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->label}}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">This is the Role that will be alerted.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>