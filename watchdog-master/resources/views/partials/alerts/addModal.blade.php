<!-- add modal -->
<div class="modal fade" id="addModal" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalTitle">Add Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-form-action" action="/alerts" method="POST">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-type" class="col-form-label">Type:</label>
                        <input type="text" class="form-control" id="add-type" name="type" required autofocus>
                        <small class="form-text text-muted">Insert a type of alert.</small>
                    </div>
                    <div class="form-group">
                        <label for="add-level" class="col-form-label">Level:</label>
                        <select class="form-control" id="add-level" name="level" required>
                            <option value="warning">Warning</option>
                            <option value="danger">Danger</option>
                        </select>
                        <small class="form-text text-muted">Select a warning level.</small>
                    </div>
                    <div class="form-group">
                        <label for="add-percent" class="col-form-label">Percent:</label>
                        <input type="text" class="form-control" id="add-percent" name="percent">
                        <small class="form-text text-muted">At this percentage (0-100) an alert will be triggered.</small>
                    </div>
                    <div class="form-group">
                        <label for="add-threshold" class="col-form-label">Threshold:</label>
                        <input type="text" class="form-control" id="add-threshold" name="threshold">
                        <small class="form-text text-muted">At this threshold an alert will be triggered.</small>
                    </div>
                    <div class="form-group">
                        <label for="add-role" class="col-form-label">Alerted Role:</label>
                        <select class="form-control" id="add-role" name="role">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->label}}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">This is the Role that will be alerted.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>