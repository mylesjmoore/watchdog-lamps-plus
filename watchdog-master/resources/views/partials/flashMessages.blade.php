
@if ($errors->any())
    <div class="alert alert-danger" role="alert" id="flash-message">
        <ul class="m-0 list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif