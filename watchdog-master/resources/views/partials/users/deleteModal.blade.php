<!-- delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalTitle">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="delete-form-action" action="" method="POST">
            @csrf
            @method('DELETE')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="delete-name" class="col-form-label">Name:</label>
                        <input type="text" class="form-control" id="delete-name" name="name" readonly>
                    </div>
                    <div class="form-group">
                        <label for="delete-label" class="col-form-label">Label:</label>
                        <input type="text" class="form-control" id="delete-label" name="label" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>