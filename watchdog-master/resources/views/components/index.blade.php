@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="d-flex justify-content-between">
        <div>
            <h1>Components:</h1>
        </div>
        <div>
            <a href="/manage" class="btn btn-secondary pr-3"><i class="material-icons">arrow_back</i> Manage </a>
            <button type="button" class="btn btn-primary pr-3" data-toggle="modal" data-target="#addModal"><i class="material-icons">add</i> Add </button>
        </div>
    </div>

    @include('partials.flashMessages')

    <div class="mt-2 row justify-content-center">
        <table class="table table-striped">
            <thead class="bg-dark text-white">
                <tr>
                    <th colspan="2" class="text-center" style="width: 16.66%">Action</th>
                    <th colspan="2" class="text-center" style="width: 16.66%">Display Order</th>
                    <th scope="col">Tag</th>
                    <th scope="col">Title</th>
                    <th scope="col">Size</th>
                    <th scope="col">Available</th>
                    <th scope="col">permission</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($components as $component)
                <tr>
                <td class="text-center">
                        <a href="#" class="text-info" title="Edit Component" 
                            data-toggle="modal" data-target="#editModal" data-id="{{$component->id}}" 
                            data-tag="{{$component->tag}}" data-title="{{$component->title}}" 
                            data-size="{{$component->size}}" data-available="{{$component->available}}" 
                            data-permission="{{$component->permission->id}}" data-action="{{$component->path()}}">
                                <i class="material-icons">edit</i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="#" class="text-danger" title="Delete Component" 
                            data-toggle="modal" data-target="#deleteModal" 
                            data-id="{{$component->id}}" data-tag="{{$component->tag}}" 
                            data-title="{{$component->title}}" data-action="{{$component->path()}}">
                                <i class="material-icons">delete</i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/components/displayOrder/{{$component->id}}/up" class="text-info" title="Move Up"><i class="material-icons">arrow_upward</i></a>
                    </td>
                    <td class="text-center">
                        <a href="/components/displayOrder/{{$component->id}}/down" class="text-info" title="Move Down"><i class="material-icons">arrow_downward</i></a>
                    </td>
                    <td scope="row">{{$component->tag}}</td>
                    <td scope="row">{{$component->title}}</td>
                    <td scope="row">{{$component->size}}</td>
                    <td scope="row">{{$component->available ? 'True' : 'False' }}</td>
                    <td scope="row">{{$component->permission->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {{ $components->links() }}

    @include('partials.components.addModal')
    @include('partials.components.deleteModal')
    @include('partials.components.editModal')
</div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection