@extends('layouts.app')

@section('content')
    
    <div class="row">
        <div class="col-sm-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <h1>Roles</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title">View and Edit Roles</h5>
                    <p>This is where you can view, update, and delete roles. You can also grant permissions to roles.</p>
                    <a href="/roles" class="btn btn-primary">Roles</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <h1>Permissions</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title">View and Edit Permissions</h5>
                    <p>This is where you can view, update, and delete Permissions.</p>
                    <a href="/permissions" class="btn btn-primary">Permissions</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <h1>Users</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title">View and Edit Users</h5>
                    <p>This is where you can view and update users. You can also assign roles to users.</p>
                    <a href="/users" class="btn btn-primary">Users</a>
                </div>
            </div>
        </div>
    </div>

@endsection