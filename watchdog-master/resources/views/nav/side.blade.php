<nav class="bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a href="/" class="nav-link {{ Request::is('/') ? 'active' : '' }}">
                    <span><img class="icon" src="/svg/dashboard.svg" width="15"> Dashboard</span>
                </a>
            </li>
            @can('see_orders',Auth::user())
            <li class="nav-item">
                <a href="/orders" class="nav-link {{ Request::is('orders') ? 'active' : '' }}">
                    <span><img class="icon" src="/svg/barChart.svg" width="15"> Orders</span>
                </a>
            </li>
            @endcan
        </ul>
    </div>
</nav>