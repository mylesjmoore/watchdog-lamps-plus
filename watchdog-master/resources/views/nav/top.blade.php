<nav class="navbar navbar-dark bg-dark navbar-expand-sm fixed-top flex-md-nowrap p-0 shadow">

    <a class="navbar-brand col-sm-3 col-md-2 mr-auto ml-4" href="/">
        <img class="mr-3" src="/svg/lampsPlusLogoWhite.svg" alt="" height="20">
        <img class="" src="/svg/watchDogWhite.svg" alt="" width="" height="20"> 
        {{ config('app.name', 'Laravel') }}
    </a>

    <ul class="navbar-nav mr-5">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                @can('see-admin',Auth::user())
                    <a class="dropdown-item" href="/admin">Admin</a>
                    <a class="dropdown-item" href="/telescope">Telescope</a>
                    <a class="dropdown-item" href="/horizon">Horizon</a>
                @endcan()

                @can('see_manage',Auth::user())
                    <a class="dropdown-item" href="/manage">Manage</a>
                @endcan()
                
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>
        </li>
    </ul>

</nav>