@extends('layouts.auth')

@section('content')
    <form class="form-signin login text-center" method="POST" action="{{ route('login') }}">
        @csrf
        
        <img class="mb-3" src="/svg/lampsPlusLogo.svg" alt="">
        <img class="mb-4" src="/svg/watchDogBlack.svg" alt="" width="90" height="90">
        <h1 class="h3 mb-3 font-weight-normal">Watch Dog</h1>

        <input type="username" id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="User Name" required autofocus>
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="password" required>
        @if ($errors->has('username'))
            <span class="invalid-feedback mb-2" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
            </label>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

    </form>
@endsection