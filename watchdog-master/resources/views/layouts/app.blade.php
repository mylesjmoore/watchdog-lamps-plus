<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1", shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Watch Dog') }}</title>

    <!-- vendor style -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- lamps plus style -->
    <link href="{{ asset('css/lamps.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        @include('nav.top')

        <div class="container-fluid">

            <div class="row">
                
                <div class="main col-sm-12 px-4">
                    
                    <main class="py-4">
                        @yield('content')
                    </main>
                    
                </div>

            </div>
            
        </div>

    </div>

    <!-- scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- page specific scripts -->
    @yield('js')
</body>
</html>