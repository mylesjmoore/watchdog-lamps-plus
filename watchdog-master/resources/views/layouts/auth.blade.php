<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1", shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'watchDog') }}</title>

    <!-- vendor style -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- lamps plus style -->
    <link href="{{ asset('css/auth.css') }}" rel="stylesheet">

</head>

<body>
    
    @yield('content')

</body>
</html>