@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="d-flex justify-content-between">
        <div>
            <h1>Alerts:</h1>
        </div>
        <div>
            <a href="/manage" class="btn btn-secondary pr-3"><i class="material-icons">arrow_back</i> Manage </a>
            @can('add_alerts',Auth::user())
            <button type="button" class="btn btn-primary pr-3" data-toggle="modal" data-target="#addModal"><i class="material-icons">add</i> Add </button>
            @endcan()            
        </div>
    </div>

    @include('partials.flashMessages')

    <div class="mt-2 row justify-content-center">
        <table class="table table-striped">
            <thead class="bg-dark text-white">
                <tr>
                    <th colspan="2" class="text-center" style="width: 16.66%">Action</th>
                    <th scope="col">Type</th>
                    <th scope="col">Level</th>
                    <th scope="col">Percent</th>
                    <th scope="col">Threshold</th>
                    <th scope="col">Alerted Role</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($alerts as $alert)
                <tr>
                    <td class="text-center">
                        <a href="#" class="text-info" title="Edit Alert" data-toggle="modal" data-target="#editModal" 
                            data-id="{{$alert->id}}" data-type="{{$alert->type}}" data-level="{{$alert->level}}" 
                            data-percent="{{$alert->percent}}" data-threshold="{{$alert->threshold}}" data-role="{{$alert->role->id}}" data-action="{{$alert->path()}}">
                                <i class="material-icons">edit</i>
                        </a>
                    </td>
                    <td class="text-center">
                        @can('add_alerts',Auth::user())
                        <a href="#" class="text-danger" title="Delete alert" data-toggle="modal" data-target="#deleteModal" 
                            data-id="{{$alert->id}}" data-type="{{$alert->type}}" data-level="{{$alert->level}}" data-action="{{$alert->path()}}">
                            <i class="material-icons">delete</i>
                        </a>
                        @endcan
                    </td>
                    <td scope="row">{{$alert->type}}</td>
                    <td scope="row">{{$alert->level}}</td>
                    <td scope="row">{{$alert->percent}}</td>
                    <td scope="row">{{$alert->threshold}}</td>
                    <td scope="row">{{$alert->role->label}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {{ $alerts->links() }}

    @include('partials.alerts.addModal')
    @include('partials.alerts.deleteModal')
    @include('partials.alerts.editModal')

</div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection