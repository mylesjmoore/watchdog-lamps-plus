@extends('layouts.app')

@section('content')

    <lamps-dashboard
        :user="{{auth()->user()}}"
        :user-permissions="{{json_encode(auth()->user()->permissions())}}"
        :components="{{json_encode($components)}}"
    ></lamps-dashboard>

@endsection