@extends('layouts.app')

@section('content')
    <div class="container">
        
        <div class="d-flex justify-content-between">
            <div>
                <h1>Users:</h1>
            </div>
            <div>
                <a href="/admin" class="btn btn-secondary pr-3"><i class="material-icons">arrow_back</i> Admin </a>
            </div>
        </div>

        @include('partials.flashMessages')

        <div class="mt-2 row justify-content-center">
            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr >
                        <th class="text-center" style="width: 16.66%">Action</th>
                        <th scope="col">Display Name</th>
                        <th scope="col">User Name</th>
                        <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td class="text-center">
                            <a href="/users/{{$user->id}}" class="text-info" title="View Roles">
                                <i class="material-icons">visibility</i>
                            </a>
                        </td>
                        <td scope="row">{{$user->name}}</td>
                        <td scope="row">{{$user->username}}</td>
                        <td scope="row">{{$user->email}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        {{ $users->links() }}

    </div>
@endsection
