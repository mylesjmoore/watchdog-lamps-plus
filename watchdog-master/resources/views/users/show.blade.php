@extends('layouts.app')

@section('content')
    <div class="container">
        
        <div class="d-flex justify-content-between">
            <div>
                <h1>{{$user->name}}'s Roles:</h1>
            </div>
            <div>
                <a href="/users" class="btn btn-secondary pr-3"><i class="material-icons">arrow_back</i> Users </a>
                <button type="button" class="btn btn-primary pr-3" data-toggle="modal" data-target="#addModal"><i class="material-icons">add</i> Add Role</button>
            </div>
        </div>

        @include('partials.flashMessages')

        <div class="mt-2 row justify-content-center">
            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr>
                        <th class="text-center" style="width: 16.66%">Action</th>
                        <th scope="col">Role Name</th>
                        <th scope="col">Role Label</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($user->roles as $role)
                    <tr>
                        <td class="text-center">
                            <a href="#" class="text-danger" title="Delete Role" data-toggle="modal" 
                                data-target="#deleteModal" data-user-id="{{$user->id}}" 
                                data-role-id="{{$role->id}}" data-name="{{$role->name}}" 
                                data-label="{{$role->label}}" data-action="/roleuser/{{$user->id}}/roles/{{$role->id}}">
                                    <i class="material-icons">delete</i>
                            </a>
                        </td>
                        <td scope="row">{{$role->name}}</td>
                        <td scope="row">{{$role->label}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        @include('partials.users.addModal')
        @include('partials.users.deleteModal')
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/editAndDeleteModal.js') }}"></script>
@endsection
