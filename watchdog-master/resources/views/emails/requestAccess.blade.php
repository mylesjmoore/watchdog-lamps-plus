@component('mail::message')

# A User Requested Access
---
<br>
### User Name: {{$user->name}}
### User Email: {{$user->email}}
### Permission: {{$permissionName}}

<br>
@component('mail::button',['url' => $userRolesUrl])
View User's Roles
@endcomponent
<br>

> To grant this user the requested permission. Assign the user a role that has the permission.

@endcomponent