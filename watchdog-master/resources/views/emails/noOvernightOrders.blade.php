@component('mail::message')


# No overnight orders from {{$start}} to {{$end}}
---
<br>

Last night's orders where reviewed and found that a period of time did not
have any orders from {{$start}} to {{$end}}.

Please review the watch dog application and order processing to determine if 
any action is needed.

<br>
@component('mail::button',['url' => $url])
Watch dog
@endcomponent
<br>

@endcomponent