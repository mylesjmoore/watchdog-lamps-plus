@component('mail::message')

# IBMi SUPPORT: REVIEW WMS PICK TICKET ORDERS

### The WMS Pick Ticket order count has a discrepancy of {{$threshold}}% or greater in comparison to the MOMS Pick Ticket order count.
This alert is triggered every half hour if the MOMS Pick Ticket order count in comparison to the WMS Pick Ticket order count being sent from the IBMi has not increased by a configured threshold.
IBMi Support should review DO/MOMS Pick Ticket processing.

### Current MOMS Pick Ticket Count:
 - {{$currentTime}}: {{$momsCount}}

### Current WMS Pick Ticket Count:
 - {{$currentTime}}: {{$wmsCount}} 

### Threshold
The threshold is currently set to {{$threshold}}%.

If you have access to edit alerts, you can change this threshold here:
@component('mail::button', ['url' => $alertUrl])
Configure Alert Threshold
@endcomponent

### Data Used:
Data is collected every 10 min using the following queries below:
```
Select count(*)
from I1INPT00AF
join Datesfl on dtcymdd = phpdcr
where dtcymd = current_date
```
 - Connection: IBMi
 - Table: I1INPT00AF
 - Count of: All Orders
 - Where dtcymd: Current Date in (yyyy-mm-dd)

```

Select count(*)
from PHPICK00
where substr(phpgdt,1,4) concat '-' concat substr(phpgdt,5,2) concat '-' concat substr(phpgdt,7,2) = current_date
```
 - Connection: WMS
 - Table: PHPICK00
 - Count of: All Orders
 - Where : Date is concatenated (yyyy-mm-dd)

@endcomponent