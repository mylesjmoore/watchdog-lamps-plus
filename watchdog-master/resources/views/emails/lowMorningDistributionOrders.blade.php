@component('mail::message')

# DOM SUPPORT: REVIEW DISTRIBUTION ORDERS

### The DOM Distribution Orders count is lower than expected for this time of day.
There are {{$count}} Distribution Orders at {{$time}}

#### Alert
This alert is triggered every half hour between 1:00am and 3:00am if the 
count of Distribution Orders in dom is lower than a threshold.

#### Threshold
The threshold is currently set to {{$threshold}}.

If you have access to edit alerts, you can change this threshold here:
@component('mail::button', ['url' => $alertUrl])
Configure Alert Threshold
@endcomponent

### Data Used:
DOM Distribution Orders Data is collected using the query below:
```
select count(tc_order_id)
from orders
where created_dttm > trunc(sysdate)
and o_facility_alias_id like 'W%'
```
 - Connection: DOM
 - Table: Orders
 - Count of: tc_order_id
 - Where facility: warehouse
 - And ordered: today

@endcomponent