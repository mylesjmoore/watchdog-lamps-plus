@component('mail::message')


# Queue Depth is high
---
<br>

Queue: {{$queueName}}
Count: {{$count}}

Please review the watch dog application and queue processing to determine if 
any action is needed.

<br>
@component('mail::button',['url' => $url])
Watch dog
@endcomponent
<br>

@endcomponent