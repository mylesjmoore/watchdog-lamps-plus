@component('mail::message')

# {{$type}} - REVIEW {{$system}} DISK UTILIZATION

### The Disk Usage amount for {{$system}} is currently at {{$diskAmount}}%.
This alert is triggered if the disk utilization amount exceeds the configured percentage of {{$threshold}}%. 
### This is a {{$type}} alert.

### Current {{$system}} Disk Usage:
 - {{$diskAmount}}%

### Configured {{$type}} Threshold
- {{$threshold}}%



@endcomponent