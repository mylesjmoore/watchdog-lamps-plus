@component('mail::message')

# DOM SUPPORT: REVIEW MOMS PICK TICKET ORDERS

### The MOMS Pick Ticket order count has a discrepancy of {{$threshold}}% or greater in comparison to DOM Distribution Orders.
This alert is triggered every half hour if the MOMS Pick Ticket order count in comparison to Distribution Orders from DOM has not increased by a configured threshold.
Dom Support should review DO/MOMS Pick Ticket processing.

### Current DO Count:
 - {{$currentTime}}: {{$domCount}}

### Current MOMS Pick Ticket Count:
 - {{$currentTime}}: {{$momsCount}} 

### Threshold
The threshold is currently set to {{$threshold}}%.

### Data Used:
Data is collected every 10 min using the following queries below:
```
select count(tc_order_id)
from orders
where created_dttm > trunc(sysdate)
and o_facility_alias_id like 'W%'
```
 - Connection: DOM
 - Table: Orders
 - Count of: tc_order_id
 - Where facility: warehouse
 - And ordered: today

```

Select count(*)
from I1INPT00AF
join Datesfl on dtcymdd = phpdcr
where dtcymd = current_date
```
 - Connection: IBMi
 - Table: I1INPT00AF
 - Count of: All Orders
 - Where dtcymd: Current Date in (yyyy-mm-dd)

@endcomponent