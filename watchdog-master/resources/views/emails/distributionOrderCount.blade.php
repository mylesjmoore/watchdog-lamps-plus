@component('mail::message')

# DOM SUPPORT: REVIEW DISTRIBUTION ORDERS

### The DOM DO count has not increased by {{$threshold}} since the last time it was checked.
This alert is triggered every half hour if the distribution order count has not increased by a configured threshold.
Dom Support should review DO processing.

### DO Count:
 - {{$previousTime}}: {{$previousCount}}
 - {{$currentTime}}: {{$currentCount}}

### Threshold
The threshold is currently set to {{$threshold}}.

If you have access to edit alerts, you can change this threshold here:
@component('mail::button', ['url' => $alertUrl])
Configure Alert Threshold
@endcomponent

### Data Used:
Data is collected every 10 min using the query below:
```
select count(tc_order_id)
from orders
where created_dttm > trunc(sysdate)
and o_facility_alias_id like 'W%'
```
 - Connection: DOM
 - Table: Orders
 - Count of: tc_order_id
 - Where facility: warehouse
 - And ordered: today

@endcomponent