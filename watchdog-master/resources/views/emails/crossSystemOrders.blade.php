@component('mail::message')

# REVIEW CROSS SYSTEM ORDERS

### The Cross System Order count for {{$orderType}} currently has {{$system2}} receiving {{$thresholdDifferencePercentage}}% of orders from {{$system1}}.
This alert is triggered if the system order count in comparison has an alarming percentage difference in the orders being received.

### Current {{$system1}} Count:
 - {{$thresholdTime}}: {{$orderCount1}}

### Current {{$system2}} Count:
 - {{$thresholdTime}}: {{$orderCount2}} 

### Percentage of Orders Received for {{$system2}} from {{$system1}}
- {{$thresholdDifferencePercentage}}%

### Configured Alert Percentage
- {{$setThreshold}}%

### Current Percentage Difference
- {{$discrepancyDifference}}%


@endcomponent