# Watchdog

#### Application Description
This application will provide the business and technical staff with notifications and other information useful in quickly resolving issues with order processing. It connects to various databases to gather data useful for determining if order data is not flowing as expected. There are batch jobs that run periodically to gather data or send notifications. There is a web interface that will let users see information useful in determining where order data may not be flowing as expected.

##### Application Admins
Application admins are able to add, update and delete: users, roles, and permissions. Admins can also assign roles to users and assign permissions to roles. Admins are defined at the local watchdog database level. 
 - Server: lpphpprod03 
 - Database Type: mysql 
 - Database: watchdog
 - Table: users
 - Column: admin

```
$ ssh vagrant@lpphpprod03
$ mysql -u root -p
$ use watchdog;
$ select users where admin = '1';
```

##### Authentication
Users will sign into the application through a web form using their network credentials. The application will attempt to bind to the network's active directory through ldap with the user provided credentials. If the bind is successful then the user will be logged into the application. Review the section below on ldap for information on how this works. If the user has never logged into the application before some basic user information will be stored in the local watchdog users database. Users will be assigned to one or many roles by one of the application's admins. A user may be assigned as many roles as necessary and will inherit all permissions each role has.

#### Initial Deployment

##### Create watchdog database
```
$ ssh vagrant@lpphpprod03
$ mysql -u root -p
$ create database watchdog;
```

##### Install Source
```
$ ssh vagrant@lpphpprod03
$ cd /usr/share/nginx/html
$ sudo git config --global http.sslVerify false
$ sudo git clone https://lpgitlab01.lpdomain.com/programmer/watchdog.git
$ cd watchdog
$ composer install
$ cp .env.example .env
$ sudo nano .env
	APP_ENV=production
	APP_URL=lpphpprod01/
	MYSQL_DB_PASSWORD=<password>
	IBM_DB_HOST=AS400
	IBM_DB_DATABASE=S10b0784
	IBM_DB_PASSWORD=<password>
$ sudo chmod -R 777 /usr/share/nginx/html/watchdog/
$ git add --all
$ git commit -m "init deployment"
$ php artisan migrate
```

##### ibm db password: 
```
select conpassword from moms.secconfl where conuser = 'PHPUSER';
```
##### mysql password: 
```
select conpassword from moms.secconfl where programref = 'LPPHPPROD03MYSQL'
```

### External Documentation
 - [Vagrant](https://www.barrykooij.com/connect-mysql-vagrant-machine/)
 - [Axios](https://github.com/axios/axios)
 - [Vue Components](https://vuejs.org/v2/guide/components.html)
 - [Vue Date Picker](https://github.com/charliekassel/vuejs-datepicker?ref=madewithvuejs.com)
 - [Vue Modal](https://github.com/euvl/vue-js-modal)

#### LDAP
When a user visits /login they will input their network username and password. When they submit this form it will post to the login controller (App\Http\Controllers\Auth\LoginController.php) at method attemptedLogin. This will get the ldap server from the enviornment or use lpdc06.lpdomain.com and [ldap_connect](https://www.php.net/manual/en/function.ldap-connect.php) to the server. The user's username will be prefixed with "lpdomain". The full domain user name will be "lpdomain\<username>". The php [ldap_bind](https://www.php.net/manual/en/function.ldap-bind.php) function is called using the ldap server, the domain user name, and the password the user provided. The appropriate php ldap extension is required to call the php ldap functions, see the list of commands below used to install the php ldap extension. 

 - [adldap2](https://adldap2.github.io/Adldap2-Laravel/#/)
 - [laravel ldap auth](https://github.com/jotaelesalinas/laravel-simple-ldap-auth)

```
$ ssh vagrant@lpphpprod03
$ sudo yum install php72-php-ldap.x86_64
$ sudo yum install php-ldap
$ sudo systemctl restart nginx
$ sudo systemctl restart php72-php-fpm.service
$ composer require adldap2/adldap2-laravel
$ php artisan vendor:publish --provider="Adldap\Laravel\AdldapServiceProvider"
$ php artisan vendor:publish --provider="Adldap\Laravel\AdldapAuthServiceProvider"
```

#### Horizon job queue monitor
Horizon provides a dashboard and code-driven config for Laravel powered Redis queues. Horizon allows you to monitor metrics of the queue system such as job throughput, runtime, and job failures.

The queue workers config is stored in /config/horizon.php

 - [horizon](https://laravel.com/docs/5.8/horizon)

##### Dev Install
```
$ composer require laravel/horizon
$ php artisan horizon:install
$ php artisan queue:failed-table
$ php artisan migrate
$ php artisan horizon:assets
```

##### Dashboard Auth
If a signed in user is an Admin, they will see the dropdown in the top right
navigation for the horizon dashboard /horizon. The Gate to visit this endpoint
requires that the user is an Admin.

##### Running Horizon in Dev
Horzion service must be running for queued jobs to run and to be monitored.
```
$ php artisan horizon
$ php artisan horizon:pause
$ php artisan horizon:continue
$ php artisan horizon:terminate
```

##### Deploying Horizon
On production a process monitor called "Supervisor" monitors Horzion to
ensure that it is running and will restart Horzion if it fails.

##### Supervisor
Supervisor is a process manager.
 - [supervisor](http://supervisord.org/)

install supervisor
```
$ sudo yum install supervisor
```
create config file
```
$ sudo touch /etc/supervisord.d/horizon.ini
```

populate config file (horizon.ini) with contents:
```
[program:horizon]
process_name=%(program_name)s
command=php /home/vagrant/html/watchdog/artisan horizon
autostart=true
autorestart=true
user=vagrant
redirect_stderr=true
stdout_logfile=/home/vagrant/html/watchdog/horizon.log
```

start supervisor on system startup
```
$ sudo systemctl enable supervisord
```

start horizon process
```
$ sudo systemctl restart supervisord
```

supervisor status
```
$ systemctl status supervisord.service
```

#### Production Upgrade Guide - (lpphpprod03)
Here are the steps to redeploy new code when adding new features or doing maintenance to this application.
```
$ ssh vagrant@lpphpprod03
$ cd ~/html/watchdog
$ php artisan down
$ git pull origin master
$ composer install
$ php artisan migrate
$ "yes"
$ npm run production
$ *Change any .env variables if necessary*
$ sudo systemctl restart supervisord
$ *Enter ssh vagrant password*
$ php artisan up
```

#### Using the QA/Staging Watchdog Server - (lpphp01)
There is a QA server that can be used to test commands outside of the local environment. 
This is located on LPPHP01. 

Once you have pushed and merged changes on Gitlab into the "Staging" branch, you are able to add your new code using the following steps below.
Here are the steps to redeploy new code when adding new features or doing maintenance to this application.
```
$ ssh vagrant@lpphp01
$ cd ~/html/watchdog
$ php artisan down
$ git pull origin staging
$ composer install
$ php artisan migrate
$ "yes"
$ npm run dev
$ *Change any .env variables if necessary*
$ sudo systemctl restart supervisord
$ *Enter ssh vagrant password*
$ php artisan up
```
The data for Cross Systems Orders/POS Queue Depth and any other batch processes inserting data into a MySQL table must be ran manually using php artisan <command>!