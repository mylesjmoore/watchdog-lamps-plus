<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * make user that is an admin
     *
     * @return User
     */
    protected function admin()
    {
        $admin = factory(User::class)->make();

        $admin->admin = true;

        return $admin;
    }
}
