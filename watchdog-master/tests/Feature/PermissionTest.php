<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Permission;

class PermissionTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function admin_can_create_a_permission()
    {
        $attributes = [
            'name' => $this->faker->word,
            'label' => $this->faker->sentence
        ];

        $this->actingAs($this->admin())
            ->post('/permissions', $attributes)
            ->assertRedirect('/permissions');

        $this->assertDatabaseHas('permissions', $attributes);

        Permission::where('name', $attributes['name'])->delete();
    }

    /** @test */
    public function admin_can_edit_a_permission()
    {
        $permission = factory(Permission::class)->create();

        $attributes = [
            'name' => $this->faker->word,
            'label' => $this->faker->sentence
        ];

        $this->actingAs($this->admin())->put("/permissions/{$permission->id}", $attributes);

        $this->assertDatabaseHas('permissions', $attributes);

        Permission::find($permission->id)->delete();
    }

    /** @test */
    public function admin_can_delete_a_permission()
    {
        $permission = factory(Permission::class)->create();

        $this->actingAs($this->admin())->delete("/permissions/{$permission->id}");
        
        $this->assertDatabaseMissing('permissions', $permission->toArray());
    }
}
