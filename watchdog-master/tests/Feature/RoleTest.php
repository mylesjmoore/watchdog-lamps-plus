<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Role;
use App\Permission;

class RoleTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function admin_can_make_a_role()
    {
        $attributes = [
            'name' => $this->faker->word,
            'label' => $this->faker->sentence
        ];

        $this->actingAs($this->admin())
            ->post('/roles', $attributes)
            ->assertRedirect('/roles');

        $this->assertDatabaseHas('roles', $attributes);

        Role::where('name', $attributes['name'])->delete();
    }

    /** @test */
    public function admin_can_edit_a_role()
    {
        $role = factory(Role::class)->create();

        $attributes = [
            'name' => $this->faker->word,
            'label' => $this->faker->sentence
        ];

        $this->actingAs($this->admin())->put("/roles/{$role->id}", $attributes);

        $this->assertDatabaseHas('roles', $attributes);

        Role::find($role->id)->delete();
    }

    /** @test */
    public function admin_can_delete_a_role()
    {
        $this->withoutExceptionHandling();
        $role = factory(Role::class)->create();

        $this->actingAs($this->admin())->delete("/roles/{$role->id}");
        
        $this->assertDatabaseMissing('roles', $role->toArray());
    }

    /** test */
    public function admin_can_assign_a_permission_to_a_role()
    {
        $permission = factory(Permission::class)->create();

        $role = factory(Role::class)->create();

        
        
        Role::find($role->id)->delete();

        Permission::find($permission->id)->delete();
        
    }
}
