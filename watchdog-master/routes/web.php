<?php

/**
 * View Routes
 */
Route::middleware('auth')->group(function() {
    Route::get('/', 'PageController@dashboard');
    Route::view('/admin', 'admin');
    Route::view('/manage', 'manage');
});

/**
 * Resource routes
 */
Route::resource('/roles', 'RoleController');
Route::resource('/permissions', 'PermissionController');
Route::resource('/users', 'UserController');
Route::resource('/alerts', 'AlertController');
Route::resource('/components', 'ComponentController');
Route::get('/components/displayOrder/{component}/{direction}', 'ComponentController@displayOrder');

Route::post('/roleuser/{user}','RoleUserController@store');
Route::delete('/roleuser/{user}/roles/{role}','RoleUserController@destroy');

Route::post('/permissionrole/{role}','PermissionRoleController@store');
Route::delete('/permissionrole/{permission}/role/{role}','PermissionRoleController@destroy');

/**
 * API Routes
 */
Route::get('/api/saleOrderCounts','SaleOrderCountController@index');
Route::get('/api/saleOrderCounts/orderTypes','SaleOrderCountController@orderTypes');

Route::get('/api/orderTotals','OrderTotalController@index');

Route::get('/api/queueDepths','QueueDepthController@index');

Route::get('/api/posQueueDepths','PosQueueDepthController@index');

Route::get('/api/alerts','AlertController@api');

Route::get('/api/me','MeController@index');

Route::get('/api/requestAccess','RequestAccessController@request');

Route::get('/api/statusIndicators','statusIndicatorController@index');

/**
 * Authentication Routes
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');