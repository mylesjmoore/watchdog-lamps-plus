<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WmsPickticketBridge extends Model
{
    protected $connection = 'wms';
    protected $table = 'PHPICK00';

    public static function todaysPickticketCount()
    {
        $today = date('Y-m-d');
        
        return self::whereRaw("substr(phpgdt,1,4) concat '-' concat substr(phpgdt,5,2) concat '-' concat substr(phpgdt,7,2) = '{$today}'")
            ->count();
            

    }
}
