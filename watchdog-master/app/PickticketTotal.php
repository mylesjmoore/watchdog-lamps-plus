<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PickticketTotal extends Model
{
    public $guarded = [];

    public function scopeOlderThan($query, $dateTime)
    {
        return $query->where('created_at', '<', $dateTime);
    }

    public function scopeNewerThan($query, $dateTime)
    {
        return $query->where('created_at', '>', $dateTime);
    }

    public function scopeToday($query)
    {
        return $query->where('created_at', '>', today());
    }
}
