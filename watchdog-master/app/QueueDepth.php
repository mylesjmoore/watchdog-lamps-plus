<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueueDepth extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'mqMessageDepths';

    public static function latestByLocation()
    {
        $locations = self::select('location')->distinct()->get();

        $queueDepths = collect();

        foreach ($locations as $location) {
            $queueDepths->push(
                self::select('id','typename','mqcount','created_at')
                ->where('location',$location->location)
                ->latest()
                ->first()
            );
        }

        return $queueDepths;
    }

    /**
     * return distinct locations
     *
     * @return collection
     */
    public static function locations()
    {
        return self::select('location')->distinct()->pluck('location');
    }

    /**
     * return last four counts by given location
     *
     * @param string $location
     * @return collection
     */
    public static function lastSixCountsByLocation($location)
    {
        return self::select('mqcount')->where('location', $location)->latest()->take(6)->pluck('mqcount');
    }
}
