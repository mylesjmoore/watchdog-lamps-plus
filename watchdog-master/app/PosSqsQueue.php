<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosSqsQueue extends Model
{
    protected $connection = 'ibmi';

    protected $table = 'posqueuetl';

    protected $clientProfile;

    protected $region;

    /**
     * create a queue with a given client profile and queue region
     * client profile must exist in ~/.aws/credentials
     *
     * @param string $clientProfile the client profile to connect with
     * @param string $region the queue region to connect to ie. us-east-1
     */
    public function __construct()
    {

        $this->region = env('POS_AWS_CLIENT_REGION', 'us-east-1');

        $this->clientProfile = env('POS_CLIENT_PROFILE', 'msqs1');

    }

    public function posQueues()
    {
        return $this->where('region', $this->region)
                    ->where('profile', $this->clientProfile)
                    ->get();
    }
}
