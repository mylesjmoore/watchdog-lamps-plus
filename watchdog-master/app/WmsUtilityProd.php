<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WmsUtilityProd extends Model
{
    protected $connection = 'wmsutilityprod';
    protected $table = 'itwmsputl';

    /**
     * get the most recent disk utilization amount
     *
     * @return collection
     */
    public static function diskUtilization()
    {
        $diskUtilization = self::select('tusgperc') 
            ->latest('tdatestmp')
            ->first(); 

        return $diskUtilization->tusgperc;
    }
}
