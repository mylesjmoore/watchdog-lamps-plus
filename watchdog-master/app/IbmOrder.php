<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IbmOrder extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'domohdgtb';

    /**
     * get sublocation and count of orders by sublocation
     *
     * @return collection
     */
    public static function totalsBySublocation()
    {
        $today = date('Ymd');

        return self::select(DB::raw('dhdfsbl as sublocation, count(*) as ibm'))
            ->join('imslocfl', 'sbsloc', '=', 'dhdfsbl')
            ->whereRaw("dhdodat = '{$today}'")
            ->whereRaw("sbloc# between 52 and 58 and sbloc# <> 55 and sbclose = 0 and dhddtyp not in ('Part Replacement', 'Exchange')")
            ->groupBy('dhdfsbl')
            ->get();
    }
}
