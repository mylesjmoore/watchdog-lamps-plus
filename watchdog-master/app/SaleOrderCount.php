<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\carbon;

class SaleOrderCount extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'saleOrderCounts';

    /**
     * get query by order type
     *
     * @param query $query
     * @param string $type
     * @return query
     */
    public function scopeOrderType($query, $type)
    {
        return $query->where('ordertype',$type);
    }

    /**
     * get query by order date in format yyyymmdd
     *
     * @param query $query
     * @param string $date 
     * @return query
     */
    public function scopeOnDate($query, $date)
    {
        return $query->where('orddate',$date);
    }

    /**
     * get query by order sublocation
     *
     * @param query $query
     * @param string $sublocation
     * @return query
     */
    public function scopeOfSublocation($query, $sublocation)
    {
        return $query->where('ordsbsloc',$sublocation);
    }

    /**
     * get query between two order dates in format yyyymmdd
     *
     * @param query $query
     * @param string $startDate
     * @param string $endDate
     * @return query
     */
    public function scopeBetweenDates($query, $startDate, $endDate)
    {
        return $query->whereBetween('orddate',[$startDate,$endDate]);
    }

    /**
     * get query by order hour 24hour time in format Hi
     *
     * @param query $query
     * @param string $hour
     * @return query
     */
    public function scopeOnHour($query, $hour)
    {
        return $query->where('ordhourmin',$hour);
    }

    /**
     * get todays count of orders by sublocation and hour format Hi.
     * default 0 if not found in database
     *
     * @param string $sublocation
     * @param string $hour
     * @return string|int
     */
    public static function todayBySublocationOnHour($sublocation,$hour)
    {
        $today = date('Ymd');
        
        return self::ofSublocation($sublocation)
                    ->onHour($hour)
                    ->onDate($today)
                    ->pluck('ordcount')
                    ->first() ?: 0;
    }

    /**
     * get average order count over the last month by sublocation for order hour provided.
     * default 0 if not found in database
     * 
     * @param string $sublocation
     * @param string $hour
     * @return string|int
     */
    public static function averageBySublocationOnHour($sublocation,$hour)
    {
        $today = (new Carbon())->tz('America/Los_Angeles')->format('Ymd');
        $oneMonthAgo = (new Carbon())->tz('America/Los_Angeles')->subMonth()->format('Ymd');

        return round(self::ofSublocation($sublocation)
                                ->betweenDates($oneMonthAgo,$today)
                                ->onhour($hour)
                                ->avg('ordcount')) ?: 0;
    }

    /**
     * get average order count over the last month for order hour provided.
     * default zero if not found in database
     * 
     * @param string $hour
     * @return string|int
     */
    public static function averageOnHour($hour)
    {
        $today = (new Carbon())->tz('America/Los_Angeles')->format('Ymd');
        $oneMonthAgo = (new Carbon())->tz('America/Los_Angeles')->subMonth()->format('Ymd');

        return round(self::betweenDates($oneMonthAgo,$today)->onhour($hour)->avg('ordcount')) ?: 0;
    }

    /**
     * get average order count for today on order hour provided
     * default zero if not found
     *
     * @param string $hour
     * @return string|int
     */
    public static function averageTodayOnHour($hour)
    {
        $today = date('Ymd');

        return round(self::onDate($today)->onhour($hour)->avg('ordcount')) ?: 0;
    }

    /**
     * get order counts and order time by type and date provided
     *
     * @param string $orderType
     * @param string $orderDate
     * @return collection
     */
    public static function byTypeOnDate($orderType, $orderDate)
    {
        return self::orderType($orderType)
                    ->onDate($orderDate)
                    ->orderBy('ordhourmin')
                    ->pluck('ordcount','ordhourmin');
    }

    /**
     * get order count and order time for all types on date provided
     *
     * @param string $orderDate
     * @return collection
     */
    public static function allTypesOnDate($orderDate)
    {
        return self::select(DB::raw('ordhourmin, sum(ordcount) as ordcount'))
                    ->where('orddate', $orderDate)
                    ->groupBy('ordhourmin')
                    ->orderBy('ordhourmin')
                    ->pluck('ordcount','ordhourmin');
    }

    /**
     * get order date, order hour, and sum of order count 
     * for previous night 9pm to midnight and today midnight to 6am
     *
     * @return collection
     */
    public static function overnightOrders()
    {
        $today = date('Ymd');
        $yesterday = (new Carbon())->tz('America/Los_Angeles')->subDays(1)->format('Ymd');

        return self::select('orddate','ordhourmin', DB::raw('sum(ordcount) as count'))
            ->where(function($query) use($yesterday) {
                $query->whereBetween('ordhourmin',['2100','2300'])->onDate($yesterday);
            })->orWhere(function($query) use($today) {
                $query->whereBetween('ordhourmin',['0','600'])->onDate($today);
            })
            ->groupBy('orddate','ordhourmin')
            ->orderBy('orddate')
            ->orderBy('ordhourmin')
            ->get();
    }
}
