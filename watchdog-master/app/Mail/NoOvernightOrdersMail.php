<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NoOvernightOrdersMail extends Mailable
{
    use Queueable, SerializesModels;

    public $start;
    public $end;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($start,$end)
    {
        $this->start = $start;
        $this->end = $end;
        $this->url = env('APP_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.noOvernightOrders');
    }
}
