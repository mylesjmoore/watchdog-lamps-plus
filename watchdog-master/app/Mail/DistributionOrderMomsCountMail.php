<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DistributionOrderMomsCountMail extends Mailable
{
    use Queueable, SerializesModels;

    public $currentTime;
    public $domCount;
    public $momsCount;
    public $threshold;
    public $alertUrl;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($current, $threshold)
    {
        $this->currentTime = $current['created_at']->format('h:i a');
        $this->threshold = $threshold;
        $this->domCount = $current['dom'];
        $this->momsCount = $current['moms'];
        $this->alertUrl = env('APP_URL') ."/alerts";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DOM Support: Review MOMS Pick Ticket Orders')
            ->markdown('emails.distributionOrderMomsPickTicketCount');
    }
}
