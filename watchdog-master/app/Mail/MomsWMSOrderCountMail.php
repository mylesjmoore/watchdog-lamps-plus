<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MomsWMSOrderCountMail extends Mailable
{
    use Queueable, SerializesModels;

    public $currentTime;
    public $momsCount;
    public $wmsCount;
    public $threshold;
    public $alertUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($current, $threshold)
    {
        $this->currentTime = $current['created_at']->format('h:i a');
        $this->threshold = $threshold;
        $this->momsCount = $current['moms'];
        $this->wmsCount = $current['wms'];
        $this->alertUrl = env('APP_URL') ."/alerts";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('IBMi Support: Review WMS Pick Ticket Orders')
            ->markdown('emails.momsWMSPickTicketOrderCount');
    }
}
