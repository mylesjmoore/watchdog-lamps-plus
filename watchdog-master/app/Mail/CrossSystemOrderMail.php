<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrossSystemOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $thresholdTime;
    public $setThreshold;
    public $orderType;
    public $orderCount1;
    public $orderCount2;
    public $system1;
    public $system2;
    public $thresholdDifferencePercentage;
    public $discrepancyDifference;
    public $alertUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$orderCount1,$orderCount2,$system1,$system2, $threshold,$thresholdTime)
    {
        $this->thresholdTime = $thresholdTime;
        $this->setThreshold = $threshold;
        $this->thresholdDifferencePercentage = round($orderCount2 / $orderCount1 * 100);
        $this->discrepancyDifference = $this->setThreshold - $this->thresholdDifferencePercentage;
        $this->orderType = $order['label'];
        $this->orderCount1 = $orderCount1;
        $this->orderCount2 = $orderCount2;
        $this->system1 = $system1;
        $this->system2 = $system2;
        $this->alertUrl = env('APP_URL') ."/alerts";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Cross System Orders Notification')
            ->markdown('emails.crossSystemOrders');
    }
}
