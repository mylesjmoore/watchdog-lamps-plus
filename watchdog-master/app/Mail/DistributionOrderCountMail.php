<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DistributionOrderCountMail extends Mailable
{
    use Queueable, SerializesModels;

    public $previousTime;
    public $currentTime;
    public $previousCount;
    public $currentCount;
    public $threshold;
    public $alertUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($previous, $current, $threshold)
    {
        $this->previousTime = $previous['created_at']->format('h:i a');
        $this->currentTime = $current['created_at']->format('h:i a');
        $this->previousCount = $previous['dom'];
        $this->currentCount = $current['dom'];
        $this->threshold = $threshold;
        $this->alertUrl = env('APP_URL') ."/alerts";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DOM Support: Review Distribution Orders')
            ->markdown('emails.distributionOrderCount');
    }
}
