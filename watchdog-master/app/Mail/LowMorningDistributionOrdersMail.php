<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LowMorningDistributionOrdersMail extends Mailable
{
    use Queueable, SerializesModels;

    public $count;
    public $time;
    public $alertUrl;
    public $threshold;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($distributionOrders, $threshold)
    {
        $this->count = $distributionOrders['dom'];
        $this->time = $distributionOrders['created_at']->format('h:i a');
        $this->threshold = $threshold;
        $this->alertUrl = env('APP_URL') ."/alerts";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DOM Support: Review Distribution Orders')
            ->markdown('emails.lowMorningDistributionOrders');
    }
}
