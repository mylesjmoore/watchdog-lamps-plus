<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestAccess extends Mailable
{
    use Queueable, SerializesModels;
 
    public $user;
    public $permissionName;
    public $userRolesUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$permissionName)
    {
        $this->user = $user;
        $this->permissionName = $permissionName;
        $this->userRolesUrl = env('APP_URL') ."/users/{$user->id}";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.requestAccess');
    }
}
