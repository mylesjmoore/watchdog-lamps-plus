<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DiskUtilizationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $system;
    public $diskAmount;
    public $threshold;
    public $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($system,$diskAmount,$threshold,$type)
    {
        $this->system = $system;
        $this->diskAmount = $diskAmount;
        $this->threshold = $threshold;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Disk Utilization Notification - ' . $this->type)
            ->markdown('emails.diskUtilization');
    }
}
