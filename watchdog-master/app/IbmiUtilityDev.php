<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IbmiUtilityDev extends Model
{
    protected $connection = 'ibmiutilitydev';
    protected $table = 'itdevutl';

    /**
     * get the most recent disk utilization amount
     *
     * @return collection
     */
    public static function diskUtilization()
    {
        $diskUtilization = self::select('usage_percent') 
            ->latest('pct_update_tmsp')
            ->first(); 

        return $diskUtilization->usage_percent;
    }
}
