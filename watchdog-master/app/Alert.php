<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $guarded = [];

    public function path()
    {
        return "/alerts/{$this->id}";
    }
    
    public function role()
    {
        return $this->hasOne(Role::class,'id','role_id');
    }

    public function scopeOfType($query,$type)
    {
        return $query->where('type',$type);
    }

    public function scopeOfLevel($query,$level)
    {
        return $query->where('level',$level);
    }

    public static function threshold($type, $default = null)
    {
        return self::ofType($type)->pluck('threshold')->first() ?: $default;
    }

    public static function percentByTypeOnly($type, $default = null)
    {
        return self::ofType($type)->pluck('percent')->first() ?: $default;
    }

    /**
     * return a percent by the type and level provided
     * returns 50 percent if none found in file
     *
     * @param string $type
     * @param string $level
     * @return string
     */
    public static function percentByTypeByLevel($type,$level)
    {
        return self::ofType($type)->ofLevel($level)->pluck('percent')->first() ?: '50';        
    }
}
