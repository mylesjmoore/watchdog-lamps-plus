<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Component;

class Permission extends Model
{
    public $fillable = ['name','label'];

    public function path()
    {
        return "/permissions/{$this->id}";
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

}
