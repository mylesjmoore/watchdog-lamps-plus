<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderTotal extends Model
{
    public $guarded = [];
    
    public static function sum()
    {
        return self::select(
            DB::raw("'0' as id, 'All' as label, sum(webdev) as webdev, sum(dom) as dom, sum(ibm) as ibm" )
        )->first();
    }
}
