<?php

namespace App\Aws;

use Aws\Sqs\SqsClient;

Class PosSqsClient
{
    /**
     * aws sqs client connection handle
     *
     * @var SqsClient
     */
    protected $client;

    /**
     * profile used to connect to aws sqs client
     * profile names found in ~/.aws/credentials
     *
     * @var string
     */
    protected $clientProfile;

    /**
     * region of the queue URL ie. us-east-1
     *
     * @var string
     */
    protected $region;

    /**
     * create a queue with a given client profile and queue region
     * client profile must exist in ~/.aws/credentials
     *
     * @param string $clientProfile the client profile to connect with
     * @param string $region the queue region to connect to ie. us-east-1
     */
    public function __construct()
    {

        $this->region = env('POS_AWS_CLIENT_REGION', 'us-east-1');

        $this->clientProfile = env('POS_CLIENT_PROFILE', 'msqs1');

        $this->client = new SqsClient([
            'profile' => $this->clientProfile,
            'region'  => $this->region,
            'version' => '2012-11-05'
        ]);

    }

    /**
     * get the attributes from a provided queue URL
     * 
     * @param string $queueUrl URL to the queue you are requesting attributes from
     * 
     * @return array an array of attributes returned from the client's attribute request
     */
    public function attributes($queueUrl)
    {
        $result = $this->client->getQueueAttributes([
                        'AttributeNames' => ['All'],
                        'QueueUrl' => $queueUrl
                    ]); 

        return $result->get('Attributes');        
        
    }

    /**
     * get the approx. count of messages on a queue from a provided queue URL
     * 
     * @param string $queueUrl URL to the queue you are requesting a count from
     * 
     * @return string approximate count of messages on the queue
     */
    public function messageCount($queueUrl)
    {
        $attributes = $this->attributes($queueUrl);

        return $attributes['ApproximateNumberOfMessages'];
    }

}