<?php

namespace App\Pagers;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class OnCallPager extends Model
{
    use Notifiable;

    protected $pagerKey;

    public function __construct()
    {
        $this->pagerKey = env('PAGER_ONCALL_KEY','83d756db75d545e6a99d42c4202acbb4');
    }

    public function routeNotificationForPagerDuty()
    {
        return $this->pagerKey;
    }

}