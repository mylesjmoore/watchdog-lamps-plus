<?php

namespace App\Pagers;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ModsquadPager extends Model
{
    use Notifiable;

    protected $pagerKey;

    public function __construct()
    {
        $this->pagerKey = env('PAGER_MODSQUAD_KEY','826ac37ac0c942579ea51438ff309a3f');
    }

    public function routeNotificationForPagerDuty()
    {
        return $this->pagerKey;
    }

}