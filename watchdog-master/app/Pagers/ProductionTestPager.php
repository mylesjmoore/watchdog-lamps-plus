<?php

namespace App\Pagers;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ProductionTestPager extends Model
{
    use Notifiable;

    protected $pagerKey;

    public function __construct()
    {
        $this->pagerKey = env('PAGER_PRODUCTION_TESTING_KEY','be76b6acd5624b899e6c5530dc3d4bb0');
    }

    public function routeNotificationForPagerDuty()
    {
        return $this->pagerKey;
    }

}