<?php

namespace App\Pagers;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DomPager extends Model
{
    use Notifiable;

    protected $pagerKey;

    public function __construct()
    {
        $this->pagerKey = env('PAGER_DOM_KEY','60a3b928374d4aa19f0b0b02a23a7d00');
    }

    public function routeNotificationForPagerDuty()
    {
        return $this->pagerKey;
    }

}
