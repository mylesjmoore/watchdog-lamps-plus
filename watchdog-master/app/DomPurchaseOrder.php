<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DomPurchaseOrder extends Model
{
    protected $connection = 'dom';
    protected $table = 'purchase_orders';

    /**
     * get dom orders by sublocation
     *
     * @return collection
     */
    public static function totalsBySublocation()
    {

        return self::select(DB::raw("po_ref_fields.ref_field3 as sublocation,
            COUNT(purchase_orders.purchase_orders_id) as dom"))
            ->leftJoin('order_type', 'order_category', '=', 'order_type_id')
            ->leftJoin('po_ref_fields', 'po_ref_fields.purchase_orders_id', '=', 'purchase_orders.purchase_orders_id')
            ->whereRaw("created_dttm > trunc(SYSDATE) and substr(po_ref_fields.ref_field3,0,2) = 90 and purchase_orders.order_category not in(162,55)")
            ->groupBy('po_ref_fields.ref_field3')
            ->get();
            
    }
}
