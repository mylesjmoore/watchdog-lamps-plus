<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DomOrder extends Model
{
    protected $connection = 'dom';
    protected $table = 'orders';

    public static function todaysPickticketCount()
    {
        return self::whereRaw("created_dttm > trunc(sysdate)")
            ->whereRaw("o_facility_alias_id like 'W%'")
            ->count('tc_order_id');
    }
}
