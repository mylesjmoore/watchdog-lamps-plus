<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach($this->getPermissions() as $permission) {
            gate::define($permission->name, function($user) use ($permission) {
                //if($user->isAdmin()) return true;
                return $user->hasRole($permission->roles);
            });
        }

        gate::define('see-admin', function($user) {
            return $user->isAdmin();
        });
    }

    /**
     * get permissions with roles
     * return empty array if permissions table doesnt exist
     *
     * @return collection
     */
    protected function getPermissions()
    {
        if( ! Schema::hasTable('permissions')) return [];

        return Permission::with('roles')->get();
    }
}
