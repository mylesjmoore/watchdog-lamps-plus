<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MomsPickTicketBridge extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'I1INPT00AF';

    public static function todaysPickticketCount()
    {
        $today = date('Y-m-d');
        
        return self::join('datesfl', 'dtcymdd', '=', 'phpdcr')
            ->whereRaw("dtcymd = '{$today}'")
            ->count();
            

    }
}
