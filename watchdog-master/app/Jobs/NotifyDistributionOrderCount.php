<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Alert;
use App\PickticketTotal;
use App\Pagers\DomPager;
use App\Mail\DistributionOrderCountMail;
use App\Mail\LowMorningDistributionOrdersMail;
use App\Notifications\DistributionOrderCountNotification;
use App\Notifications\LowMorningDistributionOrdersNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyDistributionOrderCount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $domSupport;
    public $developers;
    public $dcwms;
    public $webDevSupport;
    public $watchdogGroup;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->domSupport = env('MAIL_DOM_SUPPORT','domsupport@lampsplus.com');
        $this->developers = env('MAIL_DEVELOPERS','itibmiprogrammers@lampsplus.com');
        $this->dcwms = env('MAIL_DCWMS','dcwms@lampsplus.com');
        $this->webDevSupport = env('MAIL_WEB_DEV_SUPPORT','webdevsupport@lampsplus.com');
        $this->watchdogGroup = env('MAIL_WATCHDOG_GROUP','itibmiprogrammers@lampsplus.com');
    }

    /**
     * Send notification between before 6am if current dom DO count is not over expected
     * Threshold. After 6am, send alert if dom DO has not increased by another threshold. 
     *
     * @return void
     */
    public function handle(DomPager $domPager)
    {
        $thirtyMinAgo = now()->subMinutes(30);
        
        //get DOM DO current and previous counts
        $current = PickticketTotal::today()->newerThan($thirtyMinAgo)->latest()->first();
        $previous = PickticketTotal::today()->olderThan($thirtyMinAgo)->latest()->first();

        //return if no data found
        if(!$current || !$previous) return;

        $sixAM = new Carbon('6:00 am');
        
        //send notification if insufficent morning dom DOs
        if(now() < $sixAM) {
            $threshold = Alert::threshold('expected_dom_distribution_order_morning', 300);
            if($current['dom'] < $threshold) {
                $domPager->notify(new LowMorningDistributionOrdersNotification($current));
                \Mail::to($this->watchdogGroup)
                    //->cc([$this->developers, $this->dcwms, $this->webDevSupport])
                    ->send(new LowMorningDistributionOrdersMail($current, $threshold));
            }
            return;
        }
        
        //send notifiation if dom DOs have not increased by threshold
        $threshold = Alert::threshold('expected_dom_distribution_order_batch_increase', 1);
        if(($current['dom'] - $previous['dom']) < $threshold) {
            $domPager->notify(new DistributionOrderCountNotification($previous, $current, $threshold));
            \Mail::to($this->watchdogGroup)
                //->cc([$this->developers, $this->dcwms, $this->webDevSupport])
                ->send(new DistributionOrderCountMail($previous, $current, $threshold));
        }
    }
}
