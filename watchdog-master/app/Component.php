<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;

class Component extends Model
{
    public $guarded = [];
    
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    public function path()
    {
        return "/components/{$this->id}";
    }
}
