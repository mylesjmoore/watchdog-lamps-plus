<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosQueueDepth extends Model
{
    protected $guarded = [];

    public static function latestByQueue()
    {
        $queueNames = self::select('name')->distinct()->get();

        $queueDepths = collect();

        foreach ($queueNames as $queueName) {
            $queueDepths->push(
                self::select('id','name','depth','created_at')
                ->where('name',$queueName->name)
                ->latest()
                ->first()
            );
        }

        return $queueDepths;
    }

    /**
     * get records older than the provided date
     *
     * @param Query $query
     * @param Carbon $date
     * @return Query
     */
    public function scopeOlderThan($query, $date)
    {
        return $query->where('created_at', '<', $date);
    }
}
