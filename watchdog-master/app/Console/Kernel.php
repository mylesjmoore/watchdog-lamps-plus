<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //-----------------****Initializers****------------------------
        //============== Sales Audit Counts (Redis) =======================
        $schedule->command('lampsplus:initialize:pos:sales:audit:count')
                ->dailyAt('6:00')
                ->environments(['production']);


        
        //----------------------****Getters****------------------------
        //============== Queue Depths/Order Totals/Pickticket Totals =======================
        $schedule->command('lampsplus:get:sqs:queue:depths')->everyFiveMinutes();
        $schedule->command('lampsplus:get:order:totals')->everyMinute();
        $schedule->command('lampsplus:get:pickticket:totals')->everyTenMinutes();


        
        //-----------------****Notifications****------------------------
        //============== DOs/Pickticket Alerting =======================
        $schedule->command('lampsplus:notify:distributionOrder:count')
                ->cron('15,45 * * * *') //15 & 45 after the hour
                ->between('00:30', '16:30') //between 12:30am-4:30pm
                ->environments(['production']);

        $schedule->command('lampsplus:notify:distributionOrder:moms:threshold')
                ->hourlyAt(55)
                ->between('00:30', '17:30') //between 12:30am-5:30pm
                ->environments(['production']);   

        $schedule->command('lampsplus:notify:moms:wms:threshold')
                ->cron('5,35 * * * *') //5 & 35 after the hour
                ->between('01:00', '16:40') //between 01:00am-4:40pm
                ->environments(['production']); 

        //======= Order Discrepancy Average Count Alerting (Chart) ==========
        $schedule->command('lampsplus:notify:cross:system:order:discrepancy')
                ->everyFifteenMinutes()
                ->between('6:00', '22:00')
                ->environments(['production']);

        
        //============== Cross System Orders Alerting ================
        $schedule->command('lampsplus:notify:order:discrepancy')
                ->hourlyAt(15)
                ->between('6:00', '22:00')
                ->environments(['production']);

        
        //============== Overnight Orders Alerting ===================
        $schedule->command('lampsplus:notify:no:overnight:orders')
                ->dailyAt('6:00')
                ->environments(['production']);


        //============== Queue Depth Alerting ====================
        $schedule->command('lampsplus:notify:high:queue:depth')
                ->everyFiveMinutes()
                ->environments(['production']);


        //============== Sales Translate (POS) Alerting ==============     
        $schedule->command('lampsplus:notify:sales:audit:depth')
                ->dailyAt('11:00')
                ->environments(['production']);     
        
        $schedule->command('lampsplus:notify:sales:audit:count')
                ->hourlyAt(15)
                ->between('11:00', '18:00') 
                ->environments(['production']);      



        //============== Disk Utilization Alerting ==============    
        $schedule->command('lampsplus:notify:disk:utilization')
                ->hourlyAt(5)
                ->between('00:00', '23:30') //between 12am-11:30pm
                ->environments(['production']);         

        //-----------------****Application Maintenance****------------------------
        $schedule->command('lampsplus:prune:pos:queue:depths')->weekly();
        $schedule->command('telescope:prune')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
