<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class InitializeTransactionCounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:initialize:pos:sales:audit:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize daily sales count in Redis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::set('pos:sales:audit:processedcount', 0);
        Redis::set('pos:sales:audit:exportcount', 0);
    }
}
