<?php

namespace App\Console\Commands;

use App\Jobs\NotifyDistributionOrderCount as Job;
use Illuminate\Console\Command;

class NotifyDistributionOrderCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:distributionOrder:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification if distribution order count is lower than expected';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Job::dispatch();
    }
}
