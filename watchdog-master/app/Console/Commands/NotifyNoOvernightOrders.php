<?php

namespace App\Console\Commands;

use Carbon\carbon;
use App\SaleOrderCount;
use App\Mail\NoOvernightOrdersMail;
use App\Notifications\NoOvernightOrdersNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Pagers\OnCallPager;

class NotifyNoOvernightOrders extends Command
{

    protected $developers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:no:overnight:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify developers if there were no orders overnight';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->developers = env('MAIL_DEVELOPERS','itibmiprogrammers@lampsplus.com');
    }

    /**
     * Determine if last night had a 3 hour period where there were no orders then send email and notification
     *
     * @return mixed
     */
    public function handle(SaleOrderCount $saleOrderCount, OnCallPager $pager)
    {
        $orders = SaleOrderCount::overnightOrders();

        if(! $orders->contains('count',0)) return;

        foreach ($orders as $key => $order) {

            if(sizeof($orders) < 3) break;

            if($orders->take(3)->pluck('count')->sum() == 0) {

                $start = (new carbon($order['ordhourmin']))->format('h:i A');

                $end = (new carbon($order['ordhourmin']))->addHours(3)->format('h:i A');

                \Mail::to($this->developers)->send(new NoOvernightOrdersMail($start, $end));

                $pager->notify(new NoOvernightOrdersNotification($start, $end));

                return;
            }
            
            $orders->pull($key);
        }

    }
}
