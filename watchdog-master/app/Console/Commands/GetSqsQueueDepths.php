<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Aws\PosSqsClient;
use App\PosSqsQueue;
use App\PosQueueDepth;

class GetSqsQueueDepths extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:get:sqs:queue:depths';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get the number of messages on POS queues listed in moms.posqueuetl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PosSqsClient $client, PosSqsQueue $queue)
    {
        $now = now();

        $queues = $queue->posQueues();

        foreach ($queues as $queue) {

            PosQueueDepth::create([
                'name' => trim($queue->name),
                'type' => trim($queue->type),
                'active' => $queue->active,
                'depth' => $client->messageCount(trim($queue->url)),
                'created_at' => $now,
                'updated_at' => $now
            ]);

        }

    }

}
