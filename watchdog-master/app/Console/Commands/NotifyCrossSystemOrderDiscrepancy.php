<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Alert;
use App\OrderTotal;
use App\Pagers\OnCallPager;
use App\Mail\CrossSystemOrderMail;
use App\Notifications\CrossSystemOrderDiscrepancy;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class NotifyCrossSystemOrderDiscrepancy extends Command
{
    public $domSupport;
    public $developers;
    public $dcwms;
    public $webDevSupport;
    public $orderIssues;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:cross:system:order:discrepancy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification if there is a discrepancy between Cross System Orders(eCommerce/DOM/MOMS)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // $this->domSupport = env('MAIL_DOM_SUPPORT','domsupport@lampsplus.com');
        // $this->developers = env('MAIL_DEVELOPERS','itibmiprogrammers@lampsplus.com');
        // $this->dcwms = env('MAIL_DCWMS','dcwms@lampsplus.com');
        // $this->webDevSupport = env('MAIL_WEB_DEV_SUPPORT','webdevsupport@lampsplus.com');
        // $this->orderIssues = env('MAIL_ORDER_ISSUES','WatchDog@lampsplus.com');

        $this->orderIssues = env('MAIL_ORDER_ISSUES','rtucker@lampsplus.com');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OnCallPager $pager)
    {
        //Set sublocations to be monitored on Cross System Orders
        $sublocations = [ '9010', '9003' ];


        //Get Cross System Order totals
        $crossSystemOrders = OrderTotal::select('id','sublocation','label','webdev','dom','ibm')->whereIn('sublocation', $sublocations)->get();

        //Check each sublocation and see if there is a discrepancy between eCommerce->DOM & DOM->MOMS
        // eCommerce = webdev
        // DOM = dom
        // MOMS = ibm
        foreach($crossSystemOrders as $order)
        {
            //Get Threshold for Cross System Orders, if not set for a specific hour, then use default value of 50%
            //*** Correct format for alerts to be set on web page, is "cross_system_9010_7" for 7:00am or "cross_system_9003_14" for 2:00pm ***
            $crossSystemThreshold = 'cross_system_'. $order['sublocation'] . "_" . date("H");
            $thresholdTime =  date("h:i a");
            $threshold = Alert::percentByTypeOnly($crossSystemThreshold, 50);

            //Check eCommerce -> DOM
            if($this->percentDifferenceExceedsThreshold($order['webdev'],$order['dom'],$threshold)) 
            {
                $system1 = 'eCommerce';
                $system2 = 'DOM';
                $orderCount1 = $order['webdev'];
                $orderCount2 = $order['dom'];
                $pager->notify(new CrossSystemOrderDiscrepancy($order['label'],$order['webdev'], $order['dom'],$system1,$system2, $threshold));
                \Mail::to($this->orderIssues)
                    // ->cc([$this->domSupport])
                    //->cc([$this->domSupport, $this->dcwms, $this->webDevSupport])
                    ->send(new CrossSystemOrderMail($order,$orderCount1,$orderCount2,$system1,$system2, $threshold,$thresholdTime));
            }

            //Check DOM -> MOMS
            if($this->percentDifferenceExceedsThreshold($order['dom'],$order['ibm'],$threshold)) 
            {
                $system1 = 'DOM';
                $system2 = 'MOMS';
                $orderCount1 = $order['dom'];
                $orderCount2 = $order['ibm'];
                $pager->notify(new CrossSystemOrderDiscrepancy($order['label'],$order['dom'], $order['ibm'],$system1,$system2, $threshold));
                \Mail::to($this->orderIssues)
                    //->cc([$this->domSupport])
                    //->cc([$this->domSupport, $this->dcwms, $this->webDevSupport])
                    ->send(new CrossSystemOrderMail($order,$orderCount1,$orderCount2,$system1,$system2, $threshold,$thresholdTime));
            }
        }
    }

    /**
     * determin if the percent difference between two values provided exceeds the threshold provided
     *
     * @param string $value1
     * @param string $value2
     * @param bool $threshold
     * @return void
     */
    private function percentDifferenceExceedsThreshold($value1,$value2,$threshold)
    {
        return ($value2 / $value1 * 100) < $threshold;
    }
}
