<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Alert;
use App\PickticketTotal;
use App\Pagers\DomPager;
use App\Mail\MomsWMSOrderCountMail;
use App\Notifications\MomsWMSNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyMomsWMSOrderCount extends Command
{
    public $domSupport;
    public $developers;
    public $dcwms;
    public $webDevSupport;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:moms:wms:threshold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification if MOMS -> WMS is lower than expected threshold';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->domSupport = env('MAIL_DOM_SUPPORT','domsupport@lampsplus.com');
        // $this->developers = env('MAIL_DEVELOPERS','itibmiprogrammers@lampsplus.com');
        // $this->dcwms = env('MAIL_DCWMS','dcwms@lampsplus.com');
        // $this->webDevSupport = env('MAIL_WEB_DEV_SUPPORT','webdevsupport@lampsplus.com');
        
        $this->domSupport = 'mmoore@lampsplus.com';
        $this->developers = 'mmoore@lampsplus.com';
        $this->dcwms = 'mmoore@lampsplus.com';
        $this->webDevSupport = 'mmoore@lampsplus.com';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DomPager $domPager)
    {
         $current = PickticketTotal::today()->latest()->first();
         
        //send notification if MOMS -> WMS have not increased
        $threshold = Alert::threshold('expected_moms_wms_threshold', 50);
        if($this->percentDifferenceExceedsThreshold($current['moms'],$current['wms'],$threshold) and ($current['moms'] >= $current['wms'])) {
            //$domPager->notify(new MomsWMSNotification($current, $threshold));
            \Mail::to($this->domSupport)
                ->cc([$this->developers, $this->dcwms, $this->webDevSupport])
                ->send(new MomsWMSOrderCountMail($current, $threshold));
        }

    }


    /**
     * determin if the percent difference between two values provided exceeds the threshold provided
     *
     * @param string $value1
     * @param string $value2
     * @param bool $threshold
     * @return void
     */
    private function percentDifferenceExceedsThreshold($value1,$value2,$threshold)
    {
        return (abs($value1 - $value2) / $value1 * 100) >= $threshold;
    }
}
