<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DispatchJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:job {job} {parameter?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch a job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = trim($this->argument('job'));

        $class = "App\Jobs\\{$job}";

        if(!class_exists($class)) return $this->error("{$class} does not exist");

        $job = $this->argument('parameter') 
            ? new $class($this->argument('parameter'))
            : new $class();
        
        dispatch($job);

        return $this->info("{$class} dispatched");
    }
}
