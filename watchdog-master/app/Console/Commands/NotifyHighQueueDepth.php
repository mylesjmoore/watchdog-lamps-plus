<?php

namespace App\Console\Commands;

use App\Alert;
use App\QueueDepth;
use App\Pagers\ModsquadPager;
use Illuminate\Console\Command;
use App\Notifications\HighQueueCount;

class NotifyHighQueueDepth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:high:queue:depth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pager notification when queue depth is high';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get warning and danger thresholds from alerts table
     *
     * @return array
     */
    private function thresholds()
    {
        //get warning threshold
        $warning = Alert::select('threshold')
                    ->ofType('moms_queue_depth')
                    ->ofLevel('warning')->first();

        //get danger threshold
        $danger = Alert::select('threshold')
                ->ofType('moms_queue_depth')
                ->ofLevel('danger')->first();

        //return warning and danger thresholds or default values
        return [
            $warning ? $warning->threshold : 700,
            $danger ? $danger->threshold : 3500
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ModsquadPager $pager)
    {
        //get warning and danger thresholds
        list($warning, $danger) = $this->thresholds();

        //get locations in queue depth file
        $locations = QueueDepth::locations();

        $alertLocations = [];

        //for each location
        foreach($locations as $location) {

            //get last 6 queue depth counts
            $depths = QueueDepth::lastSixCountsByLocation($location);
            
            //if last queue depth count is over danger threshold
            if($depths[0] >= $danger) {

                //add to alert locations
                $alertLocations[$location] = $depths[0];

                //continue to next location
                continue;
            }

            //if all depths are over warning threshold and last count >= previous count
            if($depths->min() >= $warning and $depths[0] >= $depths[1]) {

                //add to alert locations
                $alertLocations[$location] = $depths[0];

            }
            
        }
        
        //if there are alert locations
        if($alertLocations) {
            
            //send pager high queue count notification with alert locations
            $pager->notify(new HighQueueCount($alertLocations));

        }

    }
}
