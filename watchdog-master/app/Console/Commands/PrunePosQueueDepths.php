<?php

namespace App\Console\Commands;

use App\Jobs\PrunePosQueueDepths as Job;
use Illuminate\Console\Command;

class PrunePosQueueDepths extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:prune:pos:queue:depths';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch job to delete old pos_queue_depths records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Job::dispatch();
    }
}
