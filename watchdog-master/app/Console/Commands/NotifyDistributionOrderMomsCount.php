<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Alert;
use App\PickticketTotal;
use App\Pagers\OnCallPager;
use App\Mail\DistributionOrderMomsCountMail;
use App\Notifications\DistributionOrderMomsNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyDistributionOrderMomsCount extends Command
{
    public $domSupport;
    public $developers;
    public $dcwms;
    public $webDevSupport;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:distributionOrder:moms:threshold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification if DOs -> MOMS is lower than expected threshold';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->developers = env('MAIL_DEVELOPERS','itibmiprogrammers@lampsplus.com');
        // $this->developers = 'mmoore@lampsplus.com';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OnCallPager $pager)
    {
         $current = PickticketTotal::today()->latest()->first();
         
        //send notification if DOM -> MOMS have not increased
        $threshold = Alert::threshold('expected_dom_moms_threshold', 50);
        
        if($this->percentDifferenceExceedsThreshold($current['dom'],$current['moms'],$threshold) and ($current['dom'] >= $current['moms'])) {
            $pager->notify(new DistributionOrderMomsNotification($current, $threshold));
            \Mail::to($this->developers)
                // ->cc([$this->developers])
                ->send(new DistributionOrderMomsCountMail($current, $threshold));
        }

    }


    /**
     * determin if the percent difference between two values provided exceeds the threshold provided
     *
     * @param string $value1
     * @param string $value2
     * @param bool $threshold
     * @return void
     */
    private function percentDifferenceExceedsThreshold($value1,$value2,$threshold)
    {
        return (abs($value1 - $value2) / $value1 * 100) >= $threshold;
    }
}
