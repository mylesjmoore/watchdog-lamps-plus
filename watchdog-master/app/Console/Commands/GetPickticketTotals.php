<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DomOrder;
use App\MomsPickTicketBridge;
use App\WmsPickticketBridge;
use App\PickticketTotal;
use Carbon\Carbon;

class GetPickticketTotals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:get:pickticket:totals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Pick Ticket totals from each database: DOM, MOMS, WMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DomOrder $domOrder, MomsPickTicketBridge $momsPickTicket, WmsPickticketBridge $wmsPickTicket)
    {
        try {
            $domCount = $domOrder::todaysPickticketCount();
            $momsPickTicketCount = $momsPickTicket::todaysPickticketCount();
            $wmsPickTicketCount = $wmsPickTicket::todaysPickticketCount();
        } catch (\Throwable $th) {
            return;
        }
        
        PickticketTotal::olderThan(Carbon::now()->subWeek())->delete();

        PickticketTotal::create([
            'dom' => $domCount,
            'moms' => $momsPickTicketCount,
            'wms' => $wmsPickTicketCount
        ]);
    }
}
