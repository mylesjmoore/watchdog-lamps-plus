<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WebDevOrder;
use App\IbmOrder;
use App\OrderTotal;
use App\DomPurchaseOrder;

class GetOrderTotals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:get:order:totals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get order totals from each database: eCommerce, DOM, MOMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * collect order totals from each system: webdev sqlsrv, dom oracle, & ibm db2
     * insert order totals into local mysql db table: order_totals
     *
     * @return mixed
     */
    public function handle(WebDevOrder $webDevOrder, IbmOrder $ibmOrder, DomPurchaseOrder $domPurchaseOrder)
    {
        try {
            $ibmOrders = $ibmOrder::totalsBySublocation();
            $domPurchaseOrders = $domPurchaseOrder::totalsBySublocation();
            $orders = $webDevOrder::totalsBySublocation();
        } catch (\Throwable $th) {
            return;
        }

        $orders = $orders->map(function($order) use($ibmOrders, $domPurchaseOrders) {

            $order->ibm = $ibmOrders->where('sublocation', $order->sublocation)->pluck('ibm')->first();

            $order->dom = $domPurchaseOrders->where('sublocation', $order->sublocation)->pluck('dom')->first();

            return $order;

        });

        OrderTotal::truncate();

        foreach ($orders as $order) {

            OrderTotal::create([
                'sublocation' => $order->sublocation,
                'label' => $order->sitename,
                'webdev' => $order->webdev, 
                'ibm' => $order->ibm,
                'dom' => $order->dom
            ]);

        }

    }
}