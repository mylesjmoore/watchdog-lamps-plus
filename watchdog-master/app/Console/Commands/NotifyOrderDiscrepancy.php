<?php

namespace App\Console\Commands;

use App\Pagers\OnCallPager;
use App\SaleOrderCount;
use App\Alert;
use App\Notifications\OrderDiscrepancy;
use Illuminate\Console\Command;
use Carbon\carbon;

class NotifyOrderDiscrepancy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:order:discrepancy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Review order counts and send a notification if there is a discrepancy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OnCallPager $pager)
    {
        $sublocations = [ '9010', '9003' ];

        $previousHour = date('H')-1 .'00';

        $threshold = Alert::percentByTypeByLevel('orders','danger');

        foreach($sublocations as $sublocation)
        {
            $averageOrders = SaleOrderCount::averageBySublocationOnHour($sublocation,$previousHour);
            $ordersLastHour = SaleOrderCount::todayBySublocationOnHour($sublocation,$previousHour);

            if($ordersLastHour >= $averageOrders) return false;

            if($this->percentDifferenceExceedsThreshold($averageOrders,$ordersLastHour,$threshold))
            {
                $pager->notify(new OrderDiscrepancy($averageOrders,$ordersLastHour,$sublocation));
            }
        }

        $averageOfAllOrders = SaleOrderCount::averageOnHour($previousHour);
        $averageOfAllOrdersToday = SaleOrderCount::averageTodayOnHour($previousHour);

        if($averageOfAllOrdersToday >= $averageOfAllOrders) return false;

        if($this->percentDifferenceExceedsThreshold($averageOfAllOrders,$averageOfAllOrdersToday,$threshold))
        {
            $pager->notify(new OrderDiscrepancy($averageOfAllOrders,$averageOfAllOrdersToday));
        }

    }

    /**
     * determin if the percent difference between two values provided exceeds the threshold provided
     *
     * @param string $value1
     * @param string $value2
     * @param bool $threshold
     * @return void
     */
    private function percentDifferenceExceedsThreshold($value1,$value2,$threshold)
    {
        return (abs($value1 - $value2) / $value1 * 100) >= $threshold;
    }
}