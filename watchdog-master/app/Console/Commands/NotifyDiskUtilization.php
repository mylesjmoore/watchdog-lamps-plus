<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Alert;
use App\IbmiUtilityDev;
use App\IbmiUtilityProd;
use App\WmsUtilityDev;
use App\WmsUtilityProd;
use App\Pagers\OnCallPager;
use App\Pagers\ModsquadPager;
use App\Mail\DiskUtilizationMail;
use App\Notifications\DiskUtilization;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyDiskUtilization extends Command
{
    public $diskUtilization;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:disk:utilization';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification if disk utility amounts across the 4 systems fall below the threshold';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->diskUtilization = env('MAIL_DISK_UTILIZATION_ISSUES','mmoore@lampsplus.com');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OnCallPager $pager, ModsquadPager $modsquadpager, IbmiUtilityDev $ibmiUtilityDev, IbmiUtilityProd $ibmiUtilityProd, WmsUtilityDev $wmsUtilityDev, WmsUtilityProd $wmsUtilityProd)
    {
        $ibmiUtilityDev = $ibmiUtilityDev::diskUtilization();
        $ibmiUtilityProd = $ibmiUtilityProd::diskUtilization();
        $wmsUtilityDev = $wmsUtilityDev::diskUtilization();
        $wmsUtilityProd = $wmsUtilityProd::diskUtilization();

        //Get thresholds for disk utilization
        list($ibmiDevWarning,$ibmiProdWarning,$wmsDevWarning,$wmsProdWarning,$ibmiDevDanger,$ibmiProdDanger,$wmsDevDanger,$wmsProdDanger) = $this->thresholds();
        
        //Check if (LPDEV) Disk Utilization is in danger, if not check if a warning needs to be sent
        if($ibmiUtilityDev > $ibmiDevDanger)
        {
            $this->dangerNotification('LPDEV',$ibmiUtilityDev,$ibmiDevDanger,$pager);
        }
        elseif($ibmiUtilityDev > $ibmiDevWarning)
        {
            $this->warningNotification('LPDEV',$ibmiUtilityDev,$ibmiDevWarning,$modsquadpager);
        }


        //Check if (LPPROD) Disk Utilization is in danger, if not check if a warning needs to be sent
        if($ibmiUtilityProd > $ibmiProdDanger)
        {
            $this->dangerNotification('LPPROD',$ibmiUtilityProd,$ibmiProdDanger,$pager);
        }
        elseif($ibmiUtilityProd > $ibmiProdWarning)
        {
            $this->warningNotification('LPPROD',$ibmiUtilityProd,$ibmiProdWarning,$modsquadpager);
        }


        //Check if (WMSTEST) Disk Utilization is in danger, if not check if a warning needs to be sent
        if($wmsUtilityDev > $wmsDevDanger)
        {
            $this->dangerNotification('WMSTEST',$wmsUtilityDev,$wmsDevDanger,$pager);
        }
        elseif($wmsUtilityDev > $wmsDevWarning)
        {
            $this->warningNotification('WMSTEST',$wmsUtilityDev,$wmsDevWarning,$modsquadpager);
        }


        //Check if (WMSPROD) Disk Utilization is in danger, if not check if a warning needs to be sent
        if($wmsUtilityProd > $wmsProdDanger)
        {
            $this->dangerNotification('WMSPROD',$wmsUtilityProd,$wmsProdDanger,$pager);
        }
        elseif($wmsUtilityProd > $wmsProdWarning)
        {
            $this->warningNotification('WMSPROD',$wmsUtilityProd,$wmsProdWarning,$modsquadpager);
        }
        

    }

    /**
     * get warning and danger thresholds from alerts table
     *
     * @return array
     */
    private function thresholds()
    {
        //get warning threshold
        $ibmiDevWarning = Alert::select('threshold')->ofType('ibmi_dev_threshold')->ofLevel('warning')->first();
        $ibmiProdWarning = Alert::select('threshold')->ofType('ibmi_prod_threshold')->ofLevel('warning')->first();
        $wmsDevWarning = Alert::select('threshold')->ofType('wms_dev_threshold')->ofLevel('warning')->first();
        $wmsProdWarning = Alert::select('threshold')->ofType('wms_prod_threshold')->ofLevel('warning')->first();

        //get danger threshold
        $ibmiDevDanger = Alert::select('threshold')->ofType('ibmi_dev_threshold')->ofLevel('danger')->first();
        $ibmiProdDanger = Alert::select('threshold')->ofType('ibmi_prod_threshold')->ofLevel('danger')->first();
        $wmsDevDanger = Alert::select('threshold')->ofType('wms_dev_threshold')->ofLevel('danger')->first();
        $wmsProdDanger = Alert::select('threshold')->ofType('wms_prod_threshold')->ofLevel('danger')->first();

        //return warning and danger thresholds or default values
        return [
            $ibmiDevWarning ? $ibmiDevWarning->threshold : 50,
            $ibmiProdWarning ? $ibmiProdWarning->threshold : 50,
            $wmsDevWarning ? $wmsDevWarning->threshold : 50,
            $wmsProdWarning ? $wmsProdWarning->threshold : 50,
            $ibmiDevDanger ? $ibmiDevDanger->threshold : 50,
            $ibmiProdDanger ? $ibmiProdDanger->threshold : 50,
            $wmsDevDanger ? $wmsDevDanger->threshold : 50,
            $wmsProdDanger ? $wmsProdDanger->threshold : 50
        ];
    }


    /**
     * Check warning values for the system
     *
     * @return array
     */
    private function warningNotification($system,$diskAmount,$threshold,$pager)
    {
        $pager->notify(new DiskUtilization($system,$diskAmount,$threshold,'Warning'));
        \Mail::to($this->diskUtilization)
            ->send(new DiskUtilizationMail($system,$diskAmount,$threshold, 'Warning'));
    }

    /**
     * Check danger values for the system
     *
     * @return array
     */
    private function dangerNotification($system,$diskAmount,$threshold,$pager)
    {
        $pager->notify(new DiskUtilization($system,$diskAmount,$threshold, 'Danger'));
        \Mail::to($this->diskUtilization)
            ->send(new DiskUtilizationMail($system,$diskAmount,$threshold, 'Danger'));
    }
    
}
