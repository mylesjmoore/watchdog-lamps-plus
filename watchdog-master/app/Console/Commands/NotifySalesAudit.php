<?php

namespace App\Console\Commands;

use App\Alert;
use App\SalesAuditOrderCount;
use App\Pagers\ModsquadPager;
use Illuminate\Console\Command;
use App\Notifications\SalesAuditCount;
use Illuminate\Support\Facades\DB;


class NotifySalesAudit extends Command
{
    protected $connection = 'ibmi';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lampsplus:notify:sales:audit:depth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pager notification when POS Transaction count is zero';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get warning and danger thresholds from alerts table
     *
     * @return array
     */
    private function thresholds()
    {
        //get warning threshold
        $warning = Alert::select('threshold')
                    ->ofType('pos_sales_count')
                    ->ofLevel('warning')->first();

        //get danger threshold
        $danger = Alert::select('threshold')
                ->ofType('pos_sales_count')
                ->ofLevel('danger')->first();

        //return warning and danger thresholds or default values
        return [
            $warning ? $warning->threshold : 1,
            $danger ? $danger->threshold : 1
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ModsquadPager $pager)
    {
        //get warning and danger thresholds
        list($warning, $danger) = $this->thresholds();
        $noTransactions = false;


        //Get POS Sales Counts 
        $processed = DB::connection('ibmi')->select(DB::raw("
            select count(*) as processedcount
            from possqshstl
            join imctrlfl on mrloc# = storeno
            where sqsmsgtype in ('Sales') and procflag = 'P'
            and date(crtdttm) = current_date
            and mrcmp# = '02' and mrstr# = 1 and mrloc# <> (0) and mrkyfl = 'ITMLOC' 
            and mrloc# < 64 and mrloc# not between 50 and 59    
            "));

        //Get Saexportfl counts     
        $export = DB::connection('ibmi')->select(DB::raw("
            select count(*) as saexportcount
            from saexportfl 
            join datesfl on dtmdcyf = substr(SARECDATA,16,10)
            join imctrlfl on char(digits(mrloc#)) = substr(sarecdata,9,2)
            where dtcymd = current_date
            and mrloc# not in (52,53,54,55,56,58)
            and mrcmp# = '02' and mrstr# = 1 and mrloc# <> (0) and mrkyfl = 'ITMLOC' 
            and mrloc# < 64 and mrloc# not between 50 and 59   
            "));

        //Check if transactions have been processed/exported
        if($processed[0]->processedcount <= $danger or $export[0]->saexportcount <= $danger) 
        {
            //there is no data flowing, send an alert
            $noTransactions = true;
        }
        
        //if there are no transactions being processed or exported
        if($noTransactions) 
        {
            //send pager high queue count notification with alert locations
            $pager->notify(new SalesAuditCount());

        }

    }


    
}
