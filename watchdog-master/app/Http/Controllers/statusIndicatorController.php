<?php

namespace App\Http\Controllers;

use PDOException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class statusIndicatorController extends Controller
{
    /**
     * get status indicators for each database: moms, webdev, dom
     *
     * @return array
     */
    public function index()
    {
        //queries will try a basic select against each database and set status & sql state message
        try {
            $webDev['status'] = DB::connection('webdev')->select(DB::raw("select 1"))
                                ? 'connected'
                                : 'disconnected';
            $webDev['message'] = 'Connection to the eCommerce database was successful.';
        } catch(PDOException $e) {
            $webDev['status'] = 'disconnected';
            $webDev['message'] = 'The watchdog application is unable to connect to the eCommerce database.';
        }

        try {
            $dom['status'] = DB::connection('dom')->select(DB::raw("select 1 from dual"))
                                ? 'connected'
                                : 'disconnected';
            $dom['message'] = 'Connection to the dom database was successful.';
        } catch(PDOException $e) {
            $dom['status'] = 'disconnected';
            $dom['message'] = 'The watchdog application is unable to connect to the dom database.';
        }

        try {
            $moms['status'] = DB::connection('ibmi')->table('sysibm.sysdummy1')->select('1')->first()
                    ? 'connected'
                    : 'disconnected';
            $moms['message'] = 'Connection to the moms database was successful.';
        } catch(PDOException $e) {
            $moms['status'] = 'disconnected';
            $moms['message'] = 'The watchdog application is unable to connect to the moms database.';
        }

        //return connection status and sql state message
        return [
            [
                "id" => 1,
                "label" => "eCommerce",
                "status" => $webDev['status'],
                "message" => $webDev['message'],
            ],[
                "id" => 2,
                "label" => "DOM",
                "status" => $dom['status'],
                "message" => $dom['message'],
            ],[
                "id" => 3,
                "label" => "MOMS",
                "status" => $moms['status'],
                "message" => $moms['message'],
            ]
        ];
    }
}
