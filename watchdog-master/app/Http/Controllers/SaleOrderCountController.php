<?php

namespace App\Http\Controllers;

use App\SaleOrderCount;
use Illuminate\Http\Request;
use Carbon\carbon;

class SaleOrderCountController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * return json of sale order counts
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $selectedDate = (new Carbon($request->date))->tz('America/Los_Angeles');
        $weekPrior = (new Carbon($request->date))->tz('America/Los_Angeles')->subDays(7);

        $selectedDateData = ($request->orderType == 'All') ?
            SaleOrderCount::allTypesOnDate($selectedDate->format('Ymd')) :
            SaleOrderCount::byTypeOnDate($request->orderType,$selectedDate->format('Ymd'));

        $weekPriorData = ($request->orderType == 'All') ?
            SaleOrderCount::allTypesOnDate($weekPrior->format('Ymd')) :
            SaleOrderCount::byTypeOnDate($request->orderType,$weekPrior->format('Ymd'));
        
        return [
            'selectedDate' => [
                'label' => $selectedDate->toFormattedDateString(),
                'data' => $selectedDateData
            ],
            'weekPrior' => [
                'label' => $weekPrior->toFormattedDateString(),
                'data' => $weekPriorData
            ],
            'chartTitle' => "Totals:  Selected Date: ".$selectedDateData->sum()."  |  Week Prior: ".$weekPriorData->sum()
        ];
        
    }

    /**
     * return json array of order types
     *
     * @return \Illuminate\Http\Response
     */
    public function orderTypes()
    {
        $orderTypes = SaleOrderCount::select('ordertype')
            ->where('ordertype','<>','')
            ->orderBy('ordertype')
            ->distinct()
            ->get();

        $orderTypes->prepend(['ordertype' => 'All']);

        return $orderTypes;
    }

}
