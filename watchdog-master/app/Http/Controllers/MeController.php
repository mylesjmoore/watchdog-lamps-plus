<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = collect(auth()->user());

        $user = $user->union([
            'permissions' => auth()->user()->permissions()
        ]);

        return $user->all();
    }
}
