<?php

namespace App\Http\Controllers;

use App\Alert;
use App\Role;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('manage',['except' => ['api']]);
    }

    /**
     * returns alerts for api
     *
     * @return \Illuminate\Http\Response
     */
    public function api(Request $request)
    {
        $alerts = new Alert;
        if($request->has('type')) $alerts = $alerts->ofType($request->type);
        if($request->has('level')) $alerts = $alerts->ofLevel($request->level);
        return $alerts->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alerts = Alert::orderBy('type')->orderBy('level')->paginate(10);
        $roles = Role::all();
        return view('alerts.index', compact('alerts','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'type' => 'required',
            'level' => 'required',
            'percent' => 'nullable|numeric|min:1|max:100',
            'threshold' => 'nullable|numeric|min:1',
            'role' => 'required',
        ]);

        Alert::create([
            'type' => $request->type,
            'level' => $request->level,
            'percent' => $request->percent,
            'threshold' => $request->threshold,
            'role_id' => $request->role,
        ]);
        
        return redirect('alerts');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alert  $alert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alert $alert)
    {
        $validatedData = $request->validate([
            'percent' => 'nullable|numeric|min:1|max:100',
            'threshold' => 'nullable|numeric|min:1',
            'role' => 'required',
        ]);

        $alert->update([
            'percent' => $request->percent,
            'threshold' => $request->threshold,
            'role_id' => $request->role,
        ]);
        
        return redirect('alerts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alert  $alert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alert $alert)
    {
        $alert->delete();
        return redirect('alerts');   
    }
}
