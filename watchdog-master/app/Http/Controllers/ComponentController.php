<?php

namespace App\Http\Controllers;

use App\Component;
use App\Permission;
use Illuminate\Http\Request;

class ComponentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $components = Component::with('permission')->orderBy('display_order')->paginate();

        $permissions = permission::all();

        return view('components.index', compact('components', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tag' => 'required',
            'title' => 'required',
            'size' => 'required',
            'available' => 'required',
            'permission' => 'required'
        ]);

        $display_order = Component::max('display_order') + 1;

        Component::create([
            'display_order' => $display_order,
            'tag' => $request->tag,
            'title' => $request->title,
            'size' => $request->size,
            'available' => $request->available,
            'permission_id' => $request->permission,
        ]);
        
        return redirect('components');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Component $component)
    {
        $request->validate([
            'tag' => 'required',
            'title' => 'required',
            'size' => 'required',
            'available' => 'required',
            'permission' => 'required'
        ]);

        $component->update([
            'tag' => $request->tag,
            'title' => $request->title,
            'size' => $request->size,
            'available' => $request->available,
            'permission_id' => $request->permission,
        ]);
        
        return redirect('components');
    }

    /**
     * change component display order given a component and the direction up or down
     *
     * @param Request $request
     * @param Component $component
     * @param string $direction
     * @return \Illuminate\Http\Response
     */
    public function displayOrder(Request $request, Component $component, $direction)
    {

        if($direction == 'up' and $component->display_order <= 1) return redirect('components');

        if($direction == 'down' and Component::max('display_order') == $component->display_order) return redirect('components');

        $direction == 'up' 
            ? Component::where('display_order', $component->display_order-1)->increment('display_order')
            : Component::where('display_order', $component->display_order+1)->decrement('display_order');
            
        $direction == 'up' 
            ? $component->decrement('display_order') 
            : $component->increment('display_order');
        
        return redirect('components');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function destroy(Component $component)
    {
        $component->delete();

        return redirect('components');   
    }
}
