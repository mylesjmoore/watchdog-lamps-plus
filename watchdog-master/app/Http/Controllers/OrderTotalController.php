<?php

namespace App\Http\Controllers;

use App\OrderTotal;
use Illuminate\Http\Request;

class OrderTotalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderTotal = OrderTotal::select('id','label','webdev','dom','ibm','created_at')->get();

        $orderTotal->prepend(OrderTotal::sum());

        return $orderTotal;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderTotal  $orderTotal
     * @return \Illuminate\Http\Response
     */
    public function show(OrderTotal $orderTotal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderTotal  $orderTotal
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderTotal $orderTotal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderTotal  $orderTotal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderTotal $orderTotal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderTotal  $orderTotal
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderTotal $orderTotal)
    {
        //
    }
}
