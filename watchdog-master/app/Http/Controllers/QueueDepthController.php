<?php

namespace App\Http\Controllers;

use App\QueueDepth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueueDepthController extends Controller
{
    /**
     * return list of queue depths
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return QueueDepth::latestByLocation();
    }

}
