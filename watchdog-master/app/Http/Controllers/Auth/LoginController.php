<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Adldap\Laravel\Facades\Adldap;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return config('ldap_auth.usernames.eloquent');
    }

    protected function attemptLogin(Request $request)
    {
        $serverName = env('LDAP_SERVER', "lpdc06.lpdomain.com");

        $ldap = ldap_connect($serverName);

        $userName = $request->username;
        $password = $request->password;

        $domainPrefix = env('LDAP_DOMAIN_PREFIX', "lpdomain");

        $domainUser = "{$domainPrefix}\\{$userName}";

        if($ldap) {

            ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
    
            //authenticate user
            $bind = @ldap_bind($ldap,$domainUser,$password); 

            if($bind) {

                //user exists with provided credentials

                //get user from database
                $user = \App\User::where('username', $userName)->first();

                if (!$user) {

                    //get ldap display name
                    $filter = "(sAMAccountName=$userName)";
                    $baseDN = env('LDAP_BASE_DN', "dc=lpdomain,dc=com");
                    
                    $result = ldap_search($ldap,$baseDN,$filter);
                    $info = ldap_get_entries($ldap, $result);

                    //create user
                    $user = new \App\User();
                    $user->username = $userName;
                    $user->name = $info[0]['displayname'][0];
                    $user->email = $info[0]['mail'][0];

                }

                //TODO: https://fullstackgeek.blogspot.com/2019/02/modify-remember-me-expired-time-laravel.html
                
                //login user
                $this->guard()->login($user, true);
                ldap_close($ldap);
                return true;
            }
    
        }

        //failed authentication
        return false;
    }

}
