<?php

namespace App\Http\Controllers;

use App\PosQueueDepth;
use Illuminate\Http\Request;

class PosQueueDepthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PosQueueDepth::latestByQueue();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PosQueueDepth  $posQueueDepth
     * @return \Illuminate\Http\Response
     */
    public function show(PosQueueDepth $posQueueDepth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PosQueueDepth  $posQueueDepth
     * @return \Illuminate\Http\Response
     */
    public function edit(PosQueueDepth $posQueueDepth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PosQueueDepth  $posQueueDepth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosQueueDepth $posQueueDepth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PosQueueDepth  $posQueueDepth
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosQueueDepth $posQueueDepth)
    {
        //
    }
}
