<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, Request $request)
    {
        $role = Role::where('name',$request->role)->first();

        if($user->hasRole($role->name)) {
            return redirect()->back()->withErrors(["The user already has role: {$role->name}"]);
        }

        $user->assignRole($role);
        
        return redirect("users/{$user->id}");
    }

    /**
     * Remove a role from a user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Role $role)
    {
        $user->unassignRole($role);

        return redirect("users/{$user->id}");
    }
    
}
