<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\RequestAccess;
use Illuminate\Http\Request;

class RequestAccessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function request(Request $request)
    {
        $requestedUser = json_decode($request->user);
        $user = User::find($requestedUser->id);

        $admins = User::where('admin',true)->get();

        foreach($admins as $admin)
        {
            \Mail::to($admin)->send(new RequestAccess($user,$request->permissionName));
        }

    }
}
