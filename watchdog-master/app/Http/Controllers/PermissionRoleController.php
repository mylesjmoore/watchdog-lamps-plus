<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class PermissionRoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Role $role, Request $request)
    {
        $permission = Permission::where('name',$request->name)->first();

        if($role->hasPermission($permission->name)) {
            return redirect()->back()->withErrors(["The role already has permission: {$permission->name}"]);
        }

        $role->givePermissionTo($permission);
        
        return redirect("roles/{$role->id}");
    }

    /**
     * Remove a role from a user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission, Role $role)
    {        
        $role->removePermissionTo($permission);
        
        return redirect("roles/{$role->id}");
    }
}
