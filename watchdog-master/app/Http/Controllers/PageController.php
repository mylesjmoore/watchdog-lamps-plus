<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Component;

class PageController extends Controller
{
    public function dashboard()
    {
        $components = Component::where('available', true)->with('permission')->orderBy('display_order')->get();

        return view('dashboard', compact('components'));
    }
}
