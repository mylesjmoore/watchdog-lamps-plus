<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'IMITEMFL';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    public $primaryKey = 'IIKWRD';
}
