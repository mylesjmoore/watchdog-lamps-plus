<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WmsUtilityDev extends Model
{
    protected $connection = 'wmsutilitydev';
    protected $table = 'itwmsutl';

    /**
     * get the most recent disk utilization amount
     *
     * @return collection
     */
    public static function diskUtilization()
    {
        $diskUtilization = self::select('tusgperc') 
            ->latest('tdatestmp')
            ->first(); 

        return $diskUtilization->tusgperc;
    }
}
