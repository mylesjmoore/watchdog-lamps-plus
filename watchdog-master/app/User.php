<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql';
    protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    public function isAdmin()
    {
        return $this->admin;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    public function unassignRole(Role $role)
    {
        return $this->roles()->detach($role);
    }

    public function hasRole($role)
    {
        if(is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    public function permissions()
    {
        $permissions = collect([]);

        foreach($this->roles as $role) {

            $permissions = $permissions->concat($role->permissions->pluck('name'));

        }

        return $permissions->unique()->values()->all();
    }

}
