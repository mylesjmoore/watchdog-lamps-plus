<?php

namespace App\Notifications;

use App\Alert;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\PagerDuty\PagerDutyChannel;
use NotificationChannels\PagerDuty\PagerDutyMessage;

class HighQueueCount extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($alertLocations)
    {
        //get danger threshold
        $dangerThreshold = Alert::ofType('moms_queue_depth')->ofLevel('danger')
                                ->pluck('threshold')->first() ?: 3500;

        //get alert locations and counts for message
        $locations = implode(', ', array_keys($alertLocations));
        $counts = implode(', ', array_values($alertLocations));

        //get highest count
        $highestCount = collect($alertLocations)->max();

        //if alert locations contains count greater then alert danger threshold
        if($highestCount >= $dangerThreshold) {

            //construct critical level message
            $this->message = "
                Moms Queue Depth is at a critical level of: {$highestCount},
                
                Sign on to the system,
                Check for QSYSOPR errors,
                Check if the system is slow,
                
                If there are no errors and the system is not slow please contact the manager on call to research the high queue count.     
            ";

            return;

        }

        //construct message
        $this->message = "
            Moms Queue Depth is Higher than usual at these locations: [{$locations}],

            Their lastest counts are: [{$counts}],
            
            Sign on to the system,
            Check for QSYSOPR errors,
            Check if the system is slow,
            
            If there are no errors and the system is not slow please contact the manager on call to research the high queue count.     
        ";

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {        
        return [PagerDutyChannel::class];
    }

    /**
     * send to pager duty
     *
     * @param mixed $notifiable
     * @return PagerDutyMessage
     */
    public function toPagerDuty($notifiable)
    {
        return PagerDutyMessage::create()->setSummary($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
