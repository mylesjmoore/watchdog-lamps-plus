<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\PagerDuty\PagerDutyChannel;
use NotificationChannels\PagerDuty\PagerDutyMessage;

class OrderDiscrepancy extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($averageOrders,$ordersLastHour,$sublocation = null)
    {
        $location = $sublocation ? "sublocation {$sublocation}" : 'all sublocations';
        
        $this->message = "
            The order count from {$location} in the last hour is lower than expected. 
            The average order count is {$averageOrders} and the order count from the last hour is {$ordersLastHour}
        ";

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PagerDutyChannel::class];
    }

    /**
     * send to pager duty
     *
     * @param mixed $notifiable
     * @return PagerDutyMessage
     */
    public function toPagerDuty($notifiable)
    {
        return PagerDutyMessage::create()->setSummary($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
