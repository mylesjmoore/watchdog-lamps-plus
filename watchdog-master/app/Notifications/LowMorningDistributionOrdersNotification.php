<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\PagerDuty\PagerDutyChannel;
use NotificationChannels\PagerDuty\PagerDutyMessage;

class LowMorningDistributionOrdersNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($distributionOrders)
    {
        $count = $distributionOrders['dom'];
        $time = $distributionOrders['created_at']->format('h:i a');
        $this->message = "
            DOM Support: Review Distribution Orders.
            There are less Distribution Orders in DOM than expected for {$time}.
            Today's Distribution Order Count in DOM: {$count}
        ";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PagerDutyChannel::class];
    }

    /**
     * send to pager duty
     *
     * @param mixed $notifiable
     * @return PagerDutyMessage
     */
    public function toPagerDuty($notifiable)
    {
        return PagerDutyMessage::create()->setSummary($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
