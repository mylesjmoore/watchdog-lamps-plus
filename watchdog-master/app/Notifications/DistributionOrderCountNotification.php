<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\PagerDuty\PagerDutyChannel;
use NotificationChannels\PagerDuty\PagerDutyMessage;

class DistributionOrderCountNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($previous, $current, $threshold)
    {
        $previousTime = $previous['created_at']->format('h:i a');
        $currentTime = $current['created_at']->format('h:i a');

        $previousCount = $previous['dom'];
        $currentCount = $current['dom'];

        $this->message = "
            DOM Support: Review Distribution Orders.
            The DOM Distribution Order count has not increased by {$threshold} since the last time it was checked.
            It was {$previousCount} at {$previousTime}.
            It is now {$currentCount} at {$currentTime}.
        ";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PagerDutyChannel::class];
    }

    /**
     * send to pager duty
     *
     * @param mixed $notifiable
     * @return PagerDutyMessage
     */
    public function toPagerDuty($notifiable)
    {
        return PagerDutyMessage::create()->setSummary($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
