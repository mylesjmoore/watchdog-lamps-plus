<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\PagerDuty\PagerDutyChannel;
use NotificationChannels\PagerDuty\PagerDutyMessage;

class CrossSystemOrderDiscrepancy extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderType, $orderCount1, $orderCount2, $system1, $system2, $threshold)
    {
        $this->message = "
            [Cross System Orders Notification]
            
            The current {$orderType} Cross System Order count for {$system2} compared to {$system1} in the current hour is lower than expected.
            It is not within the currently set threshold of {$threshold}%. 
            The current order count for {$system1} is {$orderCount1}.
            The current order count for {$system2} is {$orderCount2}.
        ";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PagerDutyChannel::class];
    }

    /**
     * send to pager duty
     *
     * @param mixed $notifiable
     * @return PagerDutyMessage
     */
    public function toPagerDuty($notifiable)
    {
        return PagerDutyMessage::create()->setSummary($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
