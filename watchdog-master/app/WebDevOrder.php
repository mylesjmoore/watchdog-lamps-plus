<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WebDevOrder extends Model
{
    protected $connection = 'webdev';
    protected $table = 'tblGlobalOrderHeader';

    /**
     * get sublocation, sitename, and count(*) grouped by sublocation
     * strip the word orders from sublocation label (sitename)
     * to get new totals on page, use command:  php artisan lampsplus:get:order:totals
     * @return collection
     */
    public static function totalsBySublocation()
    {
        $today = date('Y-m-d');
        
        return collect(
            DB::connection('webdev')->select(DB::raw("
            with individualOrders as (
                SELECT h.SubLocation as sublocation,
					c.SiteName as sitename
				FROM tblGlobalOrderHeader h WITH (NOLOCK)
					INNER JOIN TblSubLocationCode c WITH (NOLOCK)
					ON c.SubLocationCode = h.SubLocation
				WHERE CONVERT(DATE, h.OrderConvertedDate) = '{$today}'
					AND c.StoreNumber <> '55'
            ),
            openbox as (
				SELECT 
					dh.SubLocationCode AS sublocation,
					c.SiteName as sitename
				FROM tblDomOrderHeader dh WITH (NOLOCK)
					INNER JOIN TblSubLocationCode c WITH (NOLOCK)
					ON c.SubLocationCode = dh.SubLocationCode  
				WHERE CONVERT(DATE, orderdate) = '{$today}'
            ),
            sublocations as (
				Select d.SubLocationCode, d.sitename
				from TblSubLocationCode d WITH (NOLOCK)
				where d.SubLocationCode in (9020,9010,9015,9021,9004,9003,9018,9009,9022)
            
            ) 
        
            select s.SubLocationCode as sublocation, s.sitename as sitename, count(*) as webdev
            from sublocations s
            left outer join individualOrders a on a.sublocation = s.SubLocationCode 
            left outer join openbox b on b.sublocation = s.SubLocationCode
            group by s.SubLocationCode, s.sitename
            order by sitename        
            "))
        )->map(function ($order) {
            $order->sitename = str_replace(['orders','Orders'], '', $order->sitename);
            return $order;
        });
    }
}