<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('order_totals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sublocation');
            $table->string('label');
            $table->integer('webdev')->nullable();
            $table->integer('dom')->nullable();
            $table->integer('ibm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('order_totals');
    }
}
