<?php

use Faker\Generator as Faker;

$factory->define(App\QueueDepth::class, function (Faker $faker) {
    return [
        'location' => $faker->randomElement($array = array ('0254','0252')),
        'label' => $faker->word,
        'count' => $faker->randomDigitNotNull
    ];
});
