$(function () {
    $('#editModal, #deleteModal').on('show.bs.modal', function (event) {

        let data = $(event.relatedTarget).data();

        let modal = $(this);

        modal.find(':input').each(function (index, input) {

            if (data.hasOwnProperty(input.name)) {
                input.value = data[input.name]
            }

        });

        modal.find('form').attr("action", data.action);
    });
});